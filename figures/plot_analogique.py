from matplotlib import pyplot as plt
import numpy as np

plt.rcParams["font.size"] = 8

x = np.linspace(0, 10, num=100)
y = 2.5 * np.sin(x) + 2.5 + np.random.normal(0, 0.25, size=100)

plt.figure(figsize=(2.45, 1.25))
plt.plot(x, y)
plt.xlabel("Temps [s]")
plt.ylabel("Tension [V]")
plt.xlim((0, 10))
plt.yticks([0, 2.5, 5])
plt.grid()
plt.tight_layout(pad=0.25)
plt.savefig("analogique.pdf")
