from matplotlib import pyplot as plt
import numpy as np

plt.rcParams["font.size"] = 8

x = np.linspace(0, 10, num=100)
values = 5 * np.array([0, 1, 0, 1, 0, 1, 1, 0, 0, 1])
y = np.repeat(values, 10) + np.random.normal(0, 0.25, size=100)

plt.figure(figsize=(2.45, 1.25))
plt.plot(x, y)
for i, val in enumerate(values):
    plt.text(i + 0.5, 2.5, 0 if val == 0 else 1,
             horizontalalignment="center",
             verticalalignment="center",
             fontweight="bold")

plt.xlabel("Temps [s]")
plt.ylabel("Tension [V]")
plt.xlim((0, 10))
plt.yticks([0, 2.5, 5])
plt.ylim((-0.5, 5.5))
plt.grid()
plt.tight_layout(pad=0.25)
plt.savefig("numerique.pdf")
