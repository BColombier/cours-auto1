LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY bascule_D IS
  
  PORT (
    clk   : IN  STD_LOGIC;
    rst_n : IN  STD_LOGIC;
    d     : IN  STD_LOGIC;
    q     : OUT STD_LOGIC);

END bascule_D;

ARCHITECTURE comportementale OF bascule_D IS

BEGIN

  PROCESS (clk, rst_n) IS
  BEGIN
    IF rst_n = '0' THEN
      q <= '0';
    ELSIF rising_edge(clk) THEN
      q <= d;
    END IF;
  END PROCESS;

END comportementale;
