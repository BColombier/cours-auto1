LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY full_adder IS

  PORT (
    a    : IN  STD_LOGIC;
    b    : IN  STD_LOGIC;
    rin  : IN  STD_LOGIC;
    s    : OUT STD_LOGIC;
    rout : OUT STD_LOGIC);

END full_adder;

ARCHITECTURE archi OF full_adder IS

BEGIN

  s    <= a XOR b XOR rin;
  rout <= (a AND b) OR (a AND rin) OR (b AND rin);

END archi;
