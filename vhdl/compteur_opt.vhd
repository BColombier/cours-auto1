LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY compteur_opt IS

  PORT (
    clk     : IN STD_LOGIC;
    rst_n   : IN STD_LOGIC;
    ena     : IN STD_LOGIC;
    dir     : IN STD_LOGIC);

END compteur_opt;

ARCHITECTURE comport OF compteur_opt IS

  CONSTANT max : STD_LOGIC_VECTOR(3 DOWNTO 0) := "1010";
  
  SIGNAL valeur : STD_LOGIC_VECTOR(3 DOWNTO 0);

BEGIN

  PROCESS (clk, rst_n)
  BEGIN
    IF rst_n = '0' THEN
      valeur <= (OTHERS => '0');
    ELSIF rising_edge(clk) THEN
      IF ena = '1' THEN
        IF dir = '1' THEN
          IF valeur = max THEN
            valeur <= (OTHERS => '0');
          ELSE
            valeur <= STD_LOGIC_VECTOR(UNSIGNED(valeur) + 1);
          END IF;
        ELSE
          IF UNSIGNED(valeur) = 0 THEN
            valeur <= max;
          ELSE
            valeur <= STD_LOGIC_VECTOR(UNSIGNED(valeur) - 1);
          END IF;
        END IF;
      END IF;
    END IF;
  END PROCESS;

END comport;
