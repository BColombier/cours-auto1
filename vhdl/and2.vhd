LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY and2 IS
  
  PORT (
    a : IN  STD_LOGIC;
    b : IN  STD_LOGIC;
    s : OUT STD_LOGIC);

END and2;

ARCHITECTURE archi OF and2 IS

BEGIN

  s <= a AND b;

END archi;
