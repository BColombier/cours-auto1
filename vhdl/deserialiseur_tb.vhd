-------------------------------------------------------------------------------
-- Title      : Testbench for design "deserialiseur"
-- Project    : 
-------------------------------------------------------------------------------
-- File       : deserialiseur_tb.vhd
-- Author     : cb64416h  <cb64416h@empcarlabhc245>
-- Company    : 
-- Created    : 2023-07-12
-- Last update: 2023-07-12
-- Platform   : 
-- Standard   : VHDL'87
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2023 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2023-07-12  1.0      cb64416h	Created
-------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

-------------------------------------------------------------------------------

ENTITY deserialiseur_tb IS

END deserialiseur_tb;

-------------------------------------------------------------------------------

ARCHITECTURE tstbench OF deserialiseur_tb IS

  COMPONENT deserialiseur
    PORT (
      clk   : IN  STD_LOGIC;
      rst_n : IN  STD_LOGIC;
      ser   : IN  STD_LOGIC;
      par   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0));
  END COMPONENT;

  -- component ports
  SIGNAL clk   : STD_LOGIC := '1';
  SIGNAL rst_n : STD_LOGIC;
  SIGNAL ser   : STD_LOGIC;
  SIGNAL par   : STD_LOGIC_VECTOR(3 DOWNTO 0);

BEGIN

  -- component instantiation
  DUT: deserialiseur
    PORT MAP (
      clk   => clk,
      rst_n => rst_n,
      ser   => ser,
      par   => par);

  -- clock generation
  clk <= not clk after 1 ns;

  rst_n <= '0', '1' AFTER 1 ns;
  ser <= '0', '1' AFTER 5 ns, '0' AFTER 7 ns, '1' AFTER 9 ns, '0' AFTER 11 ns;
  
END tstbench;
