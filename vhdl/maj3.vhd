LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY maj3 IS
  
  PORT (
    p : IN  STD_LOGIC;      -- presidente
    s : IN  STD_LOGIC;      -- secretaire
    t : IN  STD_LOGIC;      -- trésorier
    m : OUT STD_LOGIC);     -- majorite

END maj3;

ARCHITECTURE archi OF maj3 IS

BEGIN

  m <= (t AND s) OR (p AND t) OR (p AND s);

END archi;
