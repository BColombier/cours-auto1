LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY es_bidir IS

  PORT (
    es     : INOUT STD_LOGIC;
    oe     : IN    STD_LOGIC;
    sortie : IN    STD_LOGIC;
    entree : OUT   STD_LOGIC);

END es_bidir;

ARCHITECTURE archi OF es_bidir IS

BEGIN

  es <= sortie WHEN oe = '1' ELSE
       'Z';
  entree <= es;

END archi;
