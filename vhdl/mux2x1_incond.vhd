LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY mux2x1 IS
  
  PORT (
    a : IN  STD_LOGIC;
    b : IN  STD_LOGIC;
    s : IN  STD_LOGIC;
    q : OUT STD_LOGIC);

END mux2x1;

ARCHITECTURE archi OF mux2x1 IS

BEGIN

  q <= (a AND NOT s) OR (b AND s);

END archi;
