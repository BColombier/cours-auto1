-------------------------------------------------------------------------------
-- Title      : Testbench for design "bike_ctrl"
-- Project    : 
-------------------------------------------------------------------------------
-- File       : bike_ctrl_tb.vhd
-- Author     : cb64416h  <cb64416h@empcarlabhc245>
-- Company    : 
-- Created    : 2023-07-13
-- Last update: 2023-07-13
-- Platform   : 
-- Standard   : VHDL'87
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2023 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2023-07-13  1.0      cb64416h        Created
-------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

-------------------------------------------------------------------------------

ENTITY bike_ctrl_tb IS

END bike_ctrl_tb;

-------------------------------------------------------------------------------

ARCHITECTURE testbench OF bike_ctrl_tb IS

  COMPONENT bike_ctrl
    PORT (
      clk   : IN  STD_LOGIC;
      rst_n : IN  STD_LOGIC;
      p     : IN  STD_LOGIC;
      m     : IN  STD_LOGIC;
      s     : IN  STD_LOGIC;
      v     : OUT STD_LOGIC_VECTOR(1 DOWNTO 0));
  END COMPONENT;

  -- component ports
  SIGNAL clk   : STD_LOGIC := '1';
  SIGNAL rst_n : STD_LOGIC;
  SIGNAL p     : STD_LOGIC;
  SIGNAL m     : STD_LOGIC;
  SIGNAL s     : STD_LOGIC;
  SIGNAL v     : STD_LOGIC_VECTOR(1 DOWNTO 0);

BEGIN

  -- component instantiation
  DUT : bike_ctrl
    PORT MAP (
      clk   => clk,
      rst_n => rst_n,
      p     => p,
      m     => m,
      s     => s,
      v     => v);

  -- clock generation
  clk <= NOT clk AFTER 10 ns;

  rst_n <= '0', '1' AFTER 12 ns;

  PROCESS
  BEGIN
    p <= '0';
    m <= '0';
    s <= '0';
    WAIT FOR 15 ns;
    p <= '1';
    WAIT FOR 10 ns;
    p <= '0';
    WAIT FOR 10 ns;
    m <= '1';
    WAIT FOR 10 ns;
    m <= '0';
    WAIT FOR 10 ns;
    p <= '1';
    WAIT FOR 10 ns;
    p <= '0';
    WAIT FOR 10 ns;
    s <= '1';
    WAIT FOR 10 ns;
    s <= '0';
    WAIT FOR 10 ns;
    p <= '1';
    WAIT FOR 10 ns;
    p <= '0';
    WAIT FOR 10 ns;
    p <= '1';
    WAIT FOR 10 ns;
    p <= '0';
    WAIT FOR 10 ns;
    m <= '1';
    WAIT FOR 10 ns;
    m <= '0';
    WAIT FOR 10 ns;
    p <= '1';
    WAIT FOR 10 ns;
    p <= '0';
    WAIT FOR 10 ns;
    s <= '1';
    WAIT FOR 10 ns;
    s <= '0';
    WAIT FOR 10 ns;
    p <= '1';
    WAIT FOR 10 ns;
    p <= '0';
    WAIT FOR 10 ns;
    p <= '1';
    WAIT FOR 10 ns;
    p <= '0';
    WAIT FOR 10 ns;
    p <= '1';
    WAIT FOR 10 ns;
    p <= '0';
    WAIT FOR 10 ns;
    m <= '1';
    WAIT FOR 10 ns;
    m <= '0';
    WAIT FOR 10 ns;
    p <= '1';
    WAIT FOR 10 ns;
    p <= '0';
    WAIT FOR 10 ns;
    s <= '1';
    WAIT FOR 10 ns;
    s <= '0';
    WAIT FOR 20 ns;
  END PROCESS;

END testbench;
