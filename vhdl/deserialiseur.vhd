LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY deserialiseur IS
  
  PORT (
    clk   : IN  STD_LOGIC;
    rst_n : IN  STD_LOGIC;
    ser   : IN  STD_LOGIC;
    par   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0));

END deserialiseur;

ARCHITECTURE comportement OF deserialiseur IS

  SIGNAL val : STD_LOGIC_VECTOR(3 DOWNTO 0);
  
BEGIN

  PROCESS (clk, rst_n)
  BEGIN
    IF rst_n = '0' THEN
      val <= (OTHERS => '0');
    ELSIF rising_edge(clk) THEN
      val <= ser & val(3 DOWNTO 1);
    END IF;
  END PROCESS;

  par <= val;

END comportement;
