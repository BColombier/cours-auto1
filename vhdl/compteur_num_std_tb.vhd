-------------------------------------------------------------------------------
-- Title      : Testbench for design "compteur_numeric_std"
-- Project    : 
-------------------------------------------------------------------------------
-- File       : compteur_numeric_std_tb.vhd
-- Author     : cb64416h  <cb64416h@empcarlabhc245>
-- Company    : 
-- Created    : 2023-07-12
-- Last update: 2023-07-12
-- Platform   : 
-- Standard   : VHDL'87
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2023 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2023-07-12  1.0      cb64416h        Created
-------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

-------------------------------------------------------------------------------

ENTITY compteur_numeric_std_tb IS

END compteur_numeric_std_tb;

-------------------------------------------------------------------------------

ARCHITECTURE testbench OF compteur_numeric_std_tb IS

  COMPONENT compteur_numeric_std
    PORT (
      clk   : IN  STD_LOGIC;
      rst_n : IN  STD_LOGIC;
      c     : OUT STD_LOGIC_VECTOR(1 DOWNTO 0));
  END COMPONENT;

  -- component ports
  SIGNAL clk   : STD_LOGIC := '0';
  SIGNAL rst_n : STD_LOGIC;
  SIGNAL c     : STD_LOGIC_VECTOR(1 DOWNTO 0);

BEGIN

  -- component instantiation
  DUT : compteur_numeric_std
    PORT MAP (
      clk   => clk,
      rst_n => rst_n,
      c     => c);

  -- clock generation
  clk   <= NOT clk  AFTER 10 ns;
  rst_n <= '0', '1' AFTER 7 ns;

END testbench;
