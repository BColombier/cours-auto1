PROCESS (clk, rst_n)
BEGIN
  IF rst_n = '0' THEN
    b <= '0';
  ELSIF rising_edge(clk) THEN
    b <= a;
  END IF;
END PROCESS;
