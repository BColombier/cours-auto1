LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY maj3 IS
  
  PORT (
    p : IN  STD_LOGIC; --président
    s : IN  STD_LOGIC; --secrétaire
    t : IN  STD_LOGIC; --trésorier
    m : OUT STD_LOGIC);--majorite

END maj3;

ARCHITECTURE archi OF maj3 IS

  SIGNAL votes : STD_LOGIC_VECTOR(2 DOWNTO 0);
  
BEGIN

  votes <= p & s & t;  -- concaténation
  
  WITH votes SELECT
    m <=
    '1' WHEN "110", -- président et secrétaire
    '1' WHEN "101", -- président et trésorier
    '1' WHEN "011", -- secrétaire et trésorier
    '1' WHEN "111", -- tous les trois
    '0' WHEN OTHERS;

END archi;
