-------------------------------------------------------------------------------
-- Title      : Testbench for design "compteur_opt"
-- Project    : 
-------------------------------------------------------------------------------
-- File       : compteur_opt_tb.vhd
-- Author     : cb64416h  <cb64416h@empcarlabhc245>
-- Company    : 
-- Created    : 2023-07-12
-- Last update: 2023-07-12
-- Platform   : 
-- Standard   : VHDL'87
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2023 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2023-07-12  1.0      cb64416h	Created
-------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

-------------------------------------------------------------------------------

ENTITY compteur_opt_tb IS

END compteur_opt_tb;

-------------------------------------------------------------------------------

ARCHITECTURE testbench OF compteur_opt_tb IS

  COMPONENT compteur_opt
    PORT (
      clk   : IN STD_LOGIC;
      rst_n : IN STD_LOGIC;
      ena   : IN STD_LOGIC;
      dir   : IN STD_LOGIC);
  END COMPONENT;

  -- component ports
  SIGNAL clk   : STD_LOGIC := '1';
  SIGNAL rst_n : STD_LOGIC;
  SIGNAL ena   : STD_LOGIC;
  SIGNAL dir   : STD_LOGIC;

BEGIN

  -- component instantiation
  DUT: compteur_opt
    PORT MAP (
      clk   => clk,
      rst_n => rst_n,
      ena   => ena,
      dir   => dir);

  -- clock generation
  clk <= not clk after 1 ns;

  rst_n <= '0', '1' AFTER 3 ns;
  
  ena <= '1', '0' AFTER 53 ns, '1' AFTER 59 ns;

  dir <= '1', '0' AFTER 80 ns;

END testbench;
