LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY and3 IS

  PORT (
    a : IN  STD_LOGIC;
    b : IN  STD_LOGIC;
    c : IN  STD_LOGIC;
    s : OUT STD_LOGIC);

END and3;

ARCHITECTURE archi OF and3 IS

  SIGNAL a_and_b : STD_LOGIC;

BEGIN

  a_and_b <= a AND b;
  s       <= a_and_b AND c;

END archi;
