-------------------------------------------------------------------------------
-- Title      : Testbench for design "serialiseur"
-- Project    : 
-------------------------------------------------------------------------------
-- File       : serialiseur_tb.vhd
-- Author     : cb64416h  <cb64416h@empcarlabhc245>
-- Company    : 
-- Created    : 2023-07-12
-- Last update: 2023-07-12
-- Platform   : 
-- Standard   : VHDL'87
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2023 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2023-07-12  1.0      cb64416h	Created
-------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

-------------------------------------------------------------------------------

ENTITY serialiseur_tb IS

END serialiseur_tb;

-------------------------------------------------------------------------------

ARCHITECTURE testbench OF serialiseur_tb IS

  -- component ports
  COMPONENT serialiseur
    PORT (
      clk   : IN  STD_LOGIC;
      rst_n : IN  STD_LOGIC;
      par   : IN  STD_LOGIC_VECTOR(3 DOWNTO 0);
      load  : IN  STD_LOGIC;
      ser   : OUT STD_LOGIC);
  END COMPONENT;

  SIGNAL clk   : STD_LOGIC := '1';
  SIGNAL rst_n : STD_LOGIC;
  SIGNAL par   : STD_LOGIC_VECTOR(3 DOWNTO 0);
  SIGNAL load  : STD_LOGIC;
  SIGNAL ser   : STD_LOGIC;
  
BEGIN

  -- component instantiation
  serialiseur_1: serialiseur
    PORT MAP (
      clk   => clk,
      rst_n => rst_n,
      par   => par,
      load  => load,
      ser   => ser);

  -- clock generation
  clk <= not clk after 1 ns;

  rst_n <= '0', '1' AFTER 3 ns;
  par <= "0101";
  load <= '1', '0' after 5 ns, '1' AFTER 7 ns;
  
END testbench;
