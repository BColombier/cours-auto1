LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY bike_ctrl IS

  PORT (
    clk   : IN  STD_LOGIC;
    rst_n : IN  STD_LOGIC;
    p     : IN  STD_LOGIC;
    m     : IN  STD_LOGIC;
    s     : IN  STD_LOGIC;
    v     : OUT STD_LOGIC_VECTOR(1 DOWNTO 0));

END bike_ctrl;

ARCHITECTURE comport OF bike_ctrl IS

  TYPE etat IS (off, coast, speed, boost);
  SIGNAL etat_actuel : etat;

  -- Signaux pour la synchronisation des entrées
  SIGNAL p_s : STD_LOGIC;
  SIGNAL m_s : STD_LOGIC;
  SIGNAL s_s : STD_LOGIC;

BEGIN

  PROCESS (clk, rst_n)
  BEGIN
    IF rst_n = '0' THEN
      etat_actuel <= off;
    ELSIF rising_edge(clk) THEN
      p_s <= p;
      m_s <= m;
      s_s <= s;
      CASE etat_actuel IS
        WHEN off =>
          IF p_s = '1' THEN
            etat_actuel <= coast;
          END IF;
        WHEN coast =>
          IF m_s = '1' OR s_s = '1' THEN
            etat_actuel <= off;
          ELSIF p_s = '1' THEN
            etat_actuel <= speed;
          END IF;
        WHEN speed =>
          IF s_s = '1' THEN
            etat_actuel <= off;
          ELSIF m_s = '1' THEN
            etat_actuel <= coast;
          ELSIF p_s = '1' THEN
            etat_actuel <= boost;
          END IF;
        WHEN boost =>
          IF s_s = '1' THEN
            etat_actuel <= off;
          ELSIF m_s = '1' THEN
            etat_actuel <= speed;
          END IF;
        WHEN OTHERS =>
          etat_actuel <= off;
      END CASE;
    END IF;
  END PROCESS;

  WITH etat_actuel SELECT
    v <=
    "00" WHEN off,
    "01" WHEN coast,
    "10" WHEN speed,
    "11" WHEN boost,
    "00" WHEN OTHERS;

END comport;
