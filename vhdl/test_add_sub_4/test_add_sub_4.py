import numpy as np
import random

import cocotb
from cocotb.triggers import Timer
from cocotb.types import LogicArray
from cocotb.types.range import Range

N = 1000

@cocotb.test()
async def test_add_unsigned(dut):
    for rep in range(N):
        dut.op.value = 0
        a = random.randint(0, 15)
        b = random.randint(0, 15)
        while not a + b <= 15:
            a = random.randint(0, 15)
            b = random.randint(0, 15)
        dut.a.value = a
        dut.b.value = b
        await Timer(1, units="ns")
        assert dut.s.value.integer == dut.a.value.integer + dut.b.value.integer

@cocotb.test()
async def test_add_signed(dut):
    for rep in range(N):
        dut.op.value = 0
        a = random.randint(-8, 7)
        b = random.randint(-8, 7)
        while not -8 <= a + b <= 7:
            a = random.randint(-8, 7)
            b = random.randint(-8, 7)
        dut.a.value = a
        dut.b.value = b
        await Timer(1, units="ns")
        assert dut.s.value.signed_integer == dut.a.value.signed_integer + dut.b.value.signed_integer

@cocotb.test()
async def test_sub_unsigned(dut):
    for rep in range(N):
        dut.op.value = 1
        a = random.randint(0, 15)
        b = random.randint(0, 15)
        while not 0 <= a - b <= 15:
            a = random.randint(0, 15)
            b = random.randint(0, 15)
        dut.a.value = a
        dut.b.value = b
        await Timer(1, units="ns")
        assert dut.s.value.integer == dut.a.value.integer - dut.b.value.integer

@cocotb.test()
async def test_sub_signed(dut):
    for rep in range(N):
        dut.op.value = 1
        a = random.randint(-8, 7)
        b = random.randint(-8, 7)
        while not -8 <= a - b <= 7:
            a = random.randint(-8, 7)
            b = random.randint(-8, 7)
        dut.a.value = a
        dut.b.value = b
        await Timer(1, units="ns")
        assert dut.s.value.signed_integer == dut.a.value.signed_integer - dut.b.value.signed_integer

@cocotb.test()
async def test_Z(dut):
    for rep in range(N):
        dut.op.value = 1
        n = random.randint(-8, 7)
        dut.a.value = n
        dut.b.value = n
        await Timer(1, units="ns")
        assert dut.z.value == 1

@cocotb.test()
async def test_N(dut):
    for rep in range(N):
        dut.op.value = 1
        a = random.randint(-8, 7)
        b = random.randint(-8, 7)
        while not (-8 <= a - b < 0):
            a = random.randint(-8, 7)
            b = random.randint(-8, 7)
        dut.a.value = a
        dut.b.value = b
        await Timer(1, units="ns")
        assert dut.n.value == 1

@cocotb.test()
async def test_C(dut):
    for rep in range(N):
        dut.op.value = 0
        a = random.randint(0, 15)
        b = random.randint(0, 15)
        while not (a + b > 15):
            a = random.randint(0, 15)
            b = random.randint(0, 15)
        dut.a.value = a
        dut.b.value = b
        await Timer(1, units="ns")
        assert dut.c.value == 1 and dut.a.value.integer + dut.b.value.integer != dut.s.value.integer

@cocotb.test()
async def test_V_add(dut):
    for rep in range(N):
        dut.op.value = 0
        a = random.randint(-8, 7)
        b = random.randint(-8, 7)
        while not (a + b > 7 or a + b < -8):
            a = random.randint(-8, 7)
            b = random.randint(-8, 7)
        dut.a.value = a
        dut.b.value = b
        await Timer(1, units="ns")
        assert dut.v.value == 1

@cocotb.test()
async def test_V_sub(dut):
    for rep in range(N):
        dut.op.value = 1
        a = random.randint(-8, 7)
        b = random.randint(-8, 7)
        while not (a - b > 7 or a - b < -8):
            a = random.randint(-8, 7)
            b = random.randint(-8, 7)
        dut.a.value = a
        dut.b.value = b
        await Timer(1, units="ns")
        assert dut.v.value == 1
        
