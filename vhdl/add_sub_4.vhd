LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY add_sub_4 IS

  PORT (
    a  : IN  STD_LOGIC_VECTOR(3 DOWNTO 0);
    b  : IN  STD_LOGIC_VECTOR(3 DOWNTO 0);
    op : IN  STD_LOGIC;
    s  : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    z  : OUT STD_LOGIC;
    n  : OUT STD_LOGIC;
    c  : OUT STD_LOGIC;
    v  : OUT STD_LOGIC);

END add_sub_4;

ARCHITECTURE struct OF add_sub_4 IS

  COMPONENT full_adder
    PORT (
      a    : IN  STD_LOGIC;
      b    : IN  STD_LOGIC;
      rin  : IN  STD_LOGIC;
      s    : OUT STD_LOGIC;
      rout : OUT STD_LOGIC);
  END COMPONENT;

  COMPONENT mux2x1
    PORT (
      a : IN  STD_LOGIC;
      b : IN  STD_LOGIC;
      s : IN  STD_LOGIC;
      q : OUT STD_LOGIC);
  END COMPONENT;

  SIGNAL rout_0_rin_1 : STD_LOGIC;
  SIGNAL rout_1_rin_2 : STD_LOGIC;
  SIGNAL rout_2_rin_3 : STD_LOGIC;
  SIGNAL plus_1_ornot : STD_LOGIC;
  SIGNAL inv_b_ornot  : STD_LOGIC_VECTOR(3 DOWNTO 0);
  SIGNAL not_b        : STD_LOGIC_VECTOR(3 DOWNTO 0);
  SIGNAL c_sig        : STD_LOGIC;
  SIGNAL s_sig        : STD_LOGIC_VECTOR(3 DOWNTO 0);

BEGIN

  bit_0 : full_adder
    PORT MAP (
      a    => a(0),
      b    => inv_b_ornot(0),
      rin  => plus_1_ornot,
      s    => s_sig(0),
      rout => rout_0_rin_1);

  bit_1 : full_adder
    PORT MAP (
      a    => a(1),
      b    => inv_b_ornot(1),
      rin  => rout_0_rin_1,
      s    => s_sig(1),
      rout => rout_1_rin_2);

  bit_2 : full_adder
    PORT MAP (
      a    => a(2),
      b    => inv_b_ornot(2),
      rin  => rout_1_rin_2,
      s    => s_sig(2),
      rout => rout_2_rin_3);

  bit_3 : full_adder
    PORT MAP (
      a    => a(3),
      b    => inv_b_ornot(3),
      rin  => rout_2_rin_3,
      s    => s_sig(3),
      rout => c_sig);

  inv_b_0 : mux2x1
    PORT MAP (
      a => b(0),
      b => not_b(0),
      s => op,
      q => inv_b_ornot(0));

  inv_b_1 : mux2x1
    PORT MAP (
      a => b(1),
      b => not_b(1),
      s => op,
      q => inv_b_ornot(1));

  inv_b_2 : mux2x1
    PORT MAP (
      a => b(2),
      b => not_b(2),
      s => op,
      q => inv_b_ornot(2));

  inv_b_3 : mux2x1
    PORT MAP (
      a => b(3),
      b => not_b(3),
      s => op,
      q => inv_b_ornot(3));

  plus_1 : mux2x1
    PORT MAP (
      a => '0',
      b => '1',
      s => op,
      q => plus_1_ornot);

  -- Flags
  z <= NOT(s_sig(0) OR s_sig(1) OR s_sig(2) OR s_sig(3));
  n <= s_sig(3);
  v <= rout_2_rin_3 XOR c_sig;

  -- Helper signals
  not_b <= NOT(b);
  s     <= s_sig;
  c     <= c_sig;

END struct;
