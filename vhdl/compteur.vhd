LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY compteur IS
  
  PORT (
    clk   : IN  STD_LOGIC;
    rst_n : IN  STD_LOGIC;
    c     : OUT STD_LOGIC_VECTOR(1 DOWNTO 0));

END compteur;

ARCHITECTURE comportementale OF compteur IS

  SIGNAL val : STD_LOGIC_VECTOR(1 DOWNTO 0);
  
BEGIN

  PROCESS (clk, rst_n)
  BEGIN
    IF rst_n = '0' THEN
      val <= (OTHERS => '0');
    ELSIF rising_edge(clk) THEN
      val(0) <= NOT(val(0));
      val(1) <= val(0) XOR val(1);
    END IF;
  END PROCESS;

  c <= val;

END comportementale;
