LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY serialiseur IS

  PORT (
    clk   : IN  STD_LOGIC;
    rst_n : IN  STD_LOGIC;
    par   : IN  STD_LOGIC_VECTOR(3 DOWNTO 0);
    load  : IN  STD_LOGIC;
    ser   : OUT STD_LOGIC);

END serialiseur;

ARCHITECTURE comportement OF serialiseur IS

  SIGNAL val : STD_LOGIC_VECTOR(3 DOWNTO 0);

BEGIN

  PROCESS (clk, rst_n)
  BEGIN
    IF rst_n = '0' THEN
      val <= (OTHERS => '0');
    ELSIF rising_edge(clk) THEN
      IF load = '0' THEN
        val <= par;
      ELSE
        val <= '0' & val(3 DOWNTO 1);
      END IF;
    END IF;
  END PROCESS;

  ser <= val(0);

END comportement;
