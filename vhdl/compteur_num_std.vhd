LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY compteur_num_std IS
  
  PORT (
    clk   : IN  STD_LOGIC;
    rst_n : IN  STD_LOGIC;
    c     : OUT STD_LOGIC_VECTOR(1 DOWNTO 0));

END compteur_num_std;

ARCHITECTURE comport OF compteur_num_std IS

  SIGNAL val : STD_LOGIC_VECTOR(1 DOWNTO 0);
  
BEGIN

  PROCESS (clk, rst_n)
  BEGIN
    IF rst_n = '0' THEN
      val <= (OTHERS => '0');
    ELSIF rising_edge(clk) THEN
      val <= STD_LOGIC_VECTOR(UNSIGNED(val) + 1);
    END IF;
  END PROCESS;

  c <= val;

END comport;
