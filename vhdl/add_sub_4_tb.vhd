-------------------------------------------------------------------------------
-- Title      : Testbench for design "add_sub_4"
-- Project    : 
-------------------------------------------------------------------------------
-- File       : add_sub_4_tb.vhd
-- Author     : cb64416h  <cb64416h@empcarlabhc245>
-- Company    : 
-- Created    : 2023-06-15
-- Last update: 2023-06-15
-- Platform   : 
-- Standard   : VHDL'87
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2023 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2023-06-15  1.0      cb64416h	Created
-------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

-------------------------------------------------------------------------------

ENTITY add_sub_4_tb IS

END add_sub_4_tb;

-------------------------------------------------------------------------------

ARCHITECTURE test OF add_sub_4_tb IS

  COMPONENT add_sub_4
    PORT (
      a : IN STD_LOGIC);
  END COMPONENT;

  -- component ports
  SIGNAL a : STD_LOGIC;

  -- clock
  signal Clk : std_logic := '1';

BEGIN

  -- component instantiation
  DUT: add_sub_4
    PORT MAP (
      a => a);

  -- clock generation
  Clk <= not Clk after 10 ns;

  -- waveform generation
  WaveGen_Proc: process
  begin
    -- insert signal assignments here

    wait until Clk = '1';
  end process WaveGen_Proc;

  

END test;

-------------------------------------------------------------------------------

CONFIGURATION add_sub_4_tb_test_cfg OF add_sub_4_tb IS
  FOR test
  END FOR;
END add_sub_4_tb_test_cfg;

-------------------------------------------------------------------------------
