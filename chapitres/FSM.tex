\newcommand\state[4]{
  \node [draw, ultra thick, color=rbw1, circle, minimum size=2cm, #4] (#1) {};
  \node [color=rbw1, ultra thick, outer sep=8pt] at ($(#1.north)!0.5!(#1.center)$) {#2};
  \draw [ultra thick, color=rbw1] (#1.west) to (#1.east);
  \node [outer sep=8pt, color=rbw5, align=center, font=\ttfamily] at ($(#1.south)!0.6!(#1.center)$) {#3};
}
\newcommand\statereset[4]{
  \state{#1}{#2}{#3}{#4}
  \node [draw, ultra thick, color=rbw1, circle, minimum size=2.2cm, #4] (#1) {};
}
\newcommand\transition[7]{
  \draw [-latex, ultra thick, color=rbw2] (#1.#2) to [out=#2, in=#4, #7] node [midway, #6, color=rbw4] {#5} (#3.#4);
}
\newcommand\reset[1]{
  \draw [latex-, ultra thick, color=rbw2] (#1.90) to ++(0, 1);
}

\section{Machine d'états}

\subsection{Description graphique}

\begin{frame}{Machines d'états : description graphique}
  Un système séquentiel peut être décrit par :
  \begin{enumerate}
  \item des \textcolor{rbw1}{\emph{états}} possibles,
  \item des \textcolor{rbw2}{\emph{transisitions}} possibles entre certains états,
  \item des \textcolor{rbw4}{\emph{conditions}} sous lesquelles la transition entre deux états a lieu,
  \item des \textcolor{rbw5}{\emph{valeurs de sortie}} associées à chaque état.
  \end{enumerate}
  On représente souvent cela sous la forme d'un \emph{diagramme états-transitions}.
  \vfill
  \emph{Exemple :} appuyer sur \texttt{ON} allume l'apapreil, appuyer sur \texttt{OFF} l'éteint.\\
  La LED est allumée lorsque l'appareil est en marche.
  \vfill
  \begin{columns}
    \begin{column}{0.4\textwidth}
      \centering
      \begin{tikzpicture}[ultra thick]
        \node [draw, fill=main!75, minimum size=3cm, drop shadow=black] (b) {};
        \node [draw, fill=vert] at ($(b)!0.5!(b.north west)$) {\textcolor{white}{\texttt{ON}}};
        \node [draw, fill=red] at ($(b)!0.5!(b.north east)$) {\textcolor{white}{\texttt{OFF}}};
        \node [draw, circle, fill=red] at ($(b)!0.5!(b.south)$) {};
      \end{tikzpicture}
    \end{column}
    \begin{column}{0.6\textwidth}
      \centering
      \begin{tikzpicture}[thick]
        \statereset{arret}{Arrêt}{LED=0}{}
        \state{marche}{Marche}{LED=1}{right=1 of arret}
        \transition{arret}{45}{marche}{135}{$ON$}{above}{}
        \transition{marche}{215}{arret}{325}{$OFF$}{below}{}
        \reset{arret}
      \end{tikzpicture}      
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Transitions implicites}
  La plupart du temps, on ne représente que les transitions \emph{``utiles''}.\\
  Les autres sont \emph{implicites}, et signifient que l'on reste dans \emph{l'état actuel}.
  \vfill
  \centering
  \begin{tikzpicture}[thick]
    \statereset{arret}{Arrêt}{LED=0}{}
    \state{marche}{Marche}{LED=1}{right=2 of arret}
    \transition{arret}{45}{marche}{135}{$ON$}{above}{}
    \transition{marche}{215}{arret}{325}{$OFF$}{below}{}
    \reset{arret}
    
    \transition{arret}{90+60-20}{arret}{90+60+20}{$\overline{ON}$}{left}{looseness=4}
    \transition{arret}{90+2*60-20}{arret}{90+2*60+20}{$OFF$}{left}{looseness=4}
    \transition{arret}{90+3*60-20}{arret}{90+3*60+20}{$\overline{OFF}$}{below}{looseness=4}

    \transition{marche}{-180+80-20}{marche}{-180+80+20}{$\overline{OFF}$}{below}{looseness=4}
    \transition{marche}{-180+2*80-20}{marche}{-180+2*80+20}{$\overline{ON}$}{right}{looseness=4}
    \transition{marche}{-180+3*80-20}{marche}{-180+3*80+20}{$ON$}{right}{looseness=4}

  \end{tikzpicture}
\end{frame}

\begin{exercice}{Contrôleur pour vélo électrique}
  On souhaite concevoir un \emph{contrôleur pour le moteur} d'un vélo électrique.\\
  Le moteur dispose d'une \emph{entrée V}, sur 2 bits, définissant les niveaux d'assistance : aucune (\texttt{00}), basse (\texttt{01}), haute (\texttt{10}) ou maximale (\texttt{11}).
  \vfill
  Le contrôleur dispose de \emph{quatre modes} différents :
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{itemize}
      \item \emph{Off} : pas d'assistance,
      \item \emph{Coast} : assistance basse,
      \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{itemize}
      \item \emph{Speed} : assistance haute,
      \item \emph{Boost} : assistance maximale.
      \end{itemize}
    \end{column}
  \end{columns}
  \vfill
  On contrôle le mode par l'intermédiaire de \emph{deux boutons} :
  \begin{itemize}
  \item \emph{Plus} ($P$) : passer au mode plus puissant,
  \item \emph{Moins} ($M$) : passer au mode moins puissant.
  \end{itemize}
  \vfill
  On dispose en outre d'un \emph{bouton Stop} ($S$) permettant, depuis n'importe quel mode, de couper l'assistance, c'est à dire de repasser au mode \emph{Off}.
  \vfill
  \emph{Exercice :} représenter le contrôleur, le moteur et leur connexion, puis dessiner le diagramme états-transitions du contrôleur.
\end{exercice}

\begin{exercice}{Contrôleur pour vélo électrique}
  \vfill
  \begin{center}
    \begin{tikzpicture}[thick]
      \node [draw, minimum size=3cm] (ctrl) {};
      \node [anchor=south] at (ctrl.north) {controleur};
      \node [anchor=south west] at (ctrl.150) (P) {$P$};
      \draw [latex-] (P.south west) -- ++(-1, 0);
      \node [anchor=south west] at (ctrl.180) (M) {$M$};
      \draw [latex-] (M.south west) -- ++(-1, 0);
      \node [anchor=south west] at (ctrl.210) (S) {$S$};
      \draw [latex-] (S.south west) -- ++(-1, 0);
      \node [draw, minimum size=3cm, right=3 of ctrl] (mot) {};
      \node [anchor=south] at (mot.north) {moteur};
      \node [anchor=south east] at (ctrl.east) (V) {$V$};
      \node [anchor=south west] at (mot.west) (V) {$V$};
      \draw [-latex] (ctrl) to [multiwire=2] (mot);
    \end{tikzpicture}
  \end{center}
  \vfill
\end{exercice}

\begin{exercice}{Contrôleur pour vélo électrique}
  \begin{center}
    \begin{tikzpicture}
      \statereset{off}{Off}{V="00"}{}
      \state{coast}{Coast}{V="01"}{right=5 of off}
      \state{speed}{Speed}{V="10"}{below=1.75 of coast}
      \state{boost}{Boost}{V="11"}{left=5 of speed}

      \reset{off}
      
      \transition{off}{30}{coast}{150}{$P$}{above}{}
      \transition{coast}{300}{speed}{60}{$P$}{right}{}
      \transition{speed}{210}{boost}{330}{$P$}{above}{}

      \transition{boost}{10}{speed}{170}{$M$}{above}{}
      \transition{speed}{120}{coast}{240}{$M$}{right}{}
      \transition{coast}{190}{off}{350}{$M + S$}{above}{}

      \transition{speed}{135}{off}{315}{$S$}{above right}{}
      \transition{boost}{90}{off}{270}{$S$}{left}{}
    \end{tikzpicture}
  \end{center}
\end{exercice}