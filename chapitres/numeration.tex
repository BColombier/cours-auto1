\section{Numération}

\subsection{Représentation des nombres}

\begin{frame}[t]{\insertsubsection}
  Un \emph{nombre} est découpé en ``petits morceaux'' et est donc représenté par une séquence de \emph{chiffres}. Chaque \emph{chiffre} représente une valeur.
  \vfill
  En base 10, les chiffres sont : $0$, $1$, $2$, $3$, $4$, $5$, $6$, $7$, $8$ et $9$.\\
  En base $b$, il y a $b$ chiffres, de $0$ à $b-1$.
  \vfill
  En écrivant $27$, \emph{en base 10}, on écrit $2\times 10 + 7 = \textcolor{red}{\mathbf{2}} \times \mathbf{10}^{\textcolor{main}{\mathbf{1}}} + \textcolor{red}{\mathbf{7}} \times \mathbf{10}^{\textcolor{main}{\mathbf{0}}}$
  \vfill
  \begin{columns}[t]
    \begin{column}{0.525\textwidth}
      \begin{center}
        Un \emph{nombre} s'écrit donc comme\\
        une \emph{somme} de \emph{puissances} de la \emph{base}\\
        multipliées par des \emph{chiffres} de la \emph{base}.
      \end{center}
    \end{column}
    \citise{
      \begin{column}{0.475\textwidth}
        \begin{align*}
          n &= \displaystyle\sum_{k=0}^Na_kb^k\\
            &=a_0 + a_1b + a_2b^2 + ...~+ a_Nb^N
        \end{align*}
        avec $0 \leq a_n \leq b-1$ et $b\geq2$
      \end{column}
    }
  \end{columns}
  \vfill
  Écrire $1024$ en base 10 sous cette forme :\\
  $1024 = \prof{4 \times 10^0 + 2 \times 10^1 + 0 \times 10^2 + 1 \times 10^3}$
\end{frame}

\begin{frame}[t]{D'autres bases que vous connaissez déjà}
  En français, nous utilisons parfois d'autres bases :
  \vfill
  \begin{itemize}
  \item base vingt
    \begin{itemize}
    \item $96 = 4 \times 20 + 16$
    \item [] $ \phantom{96} = 4 \times 20^1 + 16 \times 20^0$
    \item Les Français disent soixante, soixante-dix, quatre-vingt et quatre-vingt-dix,
    \item Les Suisses disent soixante, septante, huitante et nonante.
    \end{itemize}
  \end{itemize}
  \vfill
  \begin{itemize}
  \item base cent
    \begin{itemize}
    \item $1789 = 17 \times 100 + 89$
    \item [] $\phantom{1789} = 17 \times 100^1 + 89 \times 100^0$
    \item appréciée des historiens.
    \end{itemize}
  \end{itemize}
  \vfill
\end{frame}

\begin{frame}{Base binaire}
  En base binaire, il n'y a que \emph{deux chiffres} : $0$ et $1$.
  \vfill
  \begin{columns}
    \begin{column}{0.45\textwidth}
      \emph{Pourquoi} la base binaire ?\\~\\
      C'est la base la plus \emph{simple}\\
      \emph{à implanter} dans le matériel.
    \end{column}
    \begin{column}{0.5\textwidth}
      \includegraphics{numerique}
    \end{column}
  \end{columns}
  \vfill
  Le principe d'écriture des nombres est identique :\\
  un nombre s'écrit en \emph{binaire} comme une \emph{somme de puissances de 2}.
  \vfill
  Exemple : $6 = 4 + 2 = 1 \times 2^2 + 1 \times 2^1 + 0 \times 2^0 = 110$
  \vfill
  La notation \emph{indicielle} permet d'\emph{expliciter la base} et évite les confusions.
  \begin{equation*}
    6_{(10)} = 110_{(2)}
  \end{equation*}
\end{frame}

\subsection{Conversions entre bases}

\begin{frame}{Conversion de binaire vers décimal}
  \centering
  \begin{columns}
    \begin{column}{0.54\textwidth}
      \begin{tikzpicture}[thick]
        \node (2p0) {1};
        \node at ([xshift=0.05cm, yshift=0.1cm]2p0.south east) {$_{(2)}$};
        \node [left=-0.25 of 2p0] (2p1) {0};
        \node [left=-0.25 of 2p1] (2p2) {1};
        \node [left=-0.25 of 2p2] (2p3) {0};
        \node [left=-0.25 of 2p3] (2p4) {0};
        \node [left=-0.25 of 2p4] (2p5) {1};
        \node [left=-0.25 of 2p5] (2p6) {0};
        \node [left=-0.25 of 2p6] (2p7) {1};
        \draw [-stealth] (2p0) |- ++(1, -1) node [anchor=west] (fleche) {$\prof{1 \times 2^0 ~~(= 1)}$};
        \draw [-stealth] (2p1) |- ([yshift=-0.5cm]fleche.west) node [anchor=west] {$+\prof{~0 \times 2^1}$};
        \draw [-stealth] (2p2) |- ([yshift=-1.0cm]fleche.west) node [anchor=west] {$+\prof{~1 \times 2^2 ~~(= 4)}$};
        \draw [-stealth] (2p3) |- ([yshift=-1.5cm]fleche.west) node [anchor=west] {$+\prof{~0 \times 2^3}$};
        \draw [-stealth] (2p4) |- ([yshift=-2.0cm]fleche.west) node [anchor=west] {$+\prof{~0 \times 2^4}$};
        \draw [-stealth] (2p5) |- ([yshift=-2.5cm]fleche.west) node [anchor=west] {$+\prof{~1 \times 2^5 ~~(= 32)}$};
        \draw [-stealth] (2p6) |- ([yshift=-3.0cm]fleche.west) node [anchor=west] {$+\prof{~0 \times 2^6}$};
        \draw [-stealth] (2p7) |- ([yshift=-3.5cm]fleche.west) node [anchor=west] (finsomme) {$+\prof{~1 \times 2^7 ~~(= 128)}$};
        \draw (finsomme.south west) -- (finsomme.south east);
        \node [below=0 of finsomme] {$\prof{165_{(10)}}$};
      \end{tikzpicture}
    \end{column}
    \begin{column}{0.45\textwidth}
      Il est utile de connaître :
      \begin{itemize}
      \item les puissances de $2$ jusqu'à $2^{16} = 65536$
        \begin{itemize}
        \item $2^3 = 8$
        \item $2^4 = 16$
        \item $2^5 = 32$
        \item $2^6 = 64$
        \item $2^7 = 128$
        \item $2^8 = 256$
        \item ...
        \end{itemize}
      \item les ordres de grandeurs :
        \begin{itemize}
        \item $2^{10} = 1024 \simeq 10^3$ 
        \item $2^{20} \simeq (10^3)^2 \simeq 10^6$ 
        \item $2^{30} \simeq 10^9$ 
        \end{itemize}
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[t]{Conversion de décimal vers binaire}
  Comment trouver l'écriture décimale (les chiffres) de la quantité $456$ ?\\
  \emph{Divisions entières successives par la base} jusqu'à obtenir un quotient nul.
  \vfill
  \begin{itemize}
  \item \emph{Division entière par} $\emph{10}$ (la base) : $456/10$
    \begin{itemize}
    \item le \emph{quotient} donne le \emph{chiffre} $4$,
    \item on poursuit avec le \emph{reste} $56$.
    \end{itemize}
  \item Division entière par $10$ (la base) : $56/10$
    \begin{itemize}
    \item le quotient donne le chiffre $5$,
    \item on poursuit avec le reste $6$.
    \end{itemize}
  \item Division entière par $10$ (la base) : $6/10$
    \begin{itemize}
    \item le quotient vaut $0$ donc on s'arrête là,
    \item le reste donne le dernier chiffre $6$.
    \end{itemize}
  \end{itemize}
  \vfill
  \centering
  Le principe est \emph{identique} en base 2.
\end{frame}

\begin{frame}{Conversion de décimal vers binaire}
  Trouver l'écriture binaire de $456_{(10)}$\\~\\
  \textbf{Divisions entières successives par 2 :}
  \vfill
  \centering
  \begin{tikzpicture}[thick]
    % \node [] (divpar2) {\emph{Divisions entières successives par 2 :}};
    \node [] (4562) {$456 / 2$};
    \node [right=1.25 of 4562.base, anchor=base] (228) {$\prof{=228}$};
    \node [right=1.25 of 228.base, anchor=base] (r01) {$\prof{\text{reste~} 0}$};
    \node [below=0.6 of 4562.west, anchor=west] (2282) {$\prof{228 / 2}$};
    \node [below=0.6 of 2282.west, anchor=west] (1142) {$\prof{114 / 2}$};
    \node [below=0.6 of 1142.west, anchor=west] (572) {$\prof{57 / 2}$};
    \node [below=0.6 of 572.west, anchor=west] (282) {$\prof{28 / 2}$};
    \node [below=0.6 of 282.west, anchor=west] (142) {$\prof{14 / 2}$};
    \node [below=0.6 of 142.west, anchor=west] (72) {$\prof{7 / 2}$};
    \node [below=0.6 of 72.west, anchor=west] (32) {$\prof{3 / 2}$};
    \node [below=0.6 of 32.west, anchor=west] (12) {$\prof{1 / 2}$};

    \node [anchor=base west] at (2282.base -| 228.west) {$\prof{=114}$};
    \node [anchor=base west] at (1142.base -| 228.west) {$\prof{=57}$};
    \node [anchor=base west] at (572.base -| 228.west) {$\prof{=28}$};
    \node [anchor=base west] at (282.base -| 228.west) {$\prof{=14}$};
    \node [anchor=base west] at (142.base -| 228.west) {$\prof{=7}$};
    \node [anchor=base west] at (72.base -| 228.west) {$\prof{=3}$};
    \node [anchor=base west] at (32.base -| 228.west) {$\prof{=1}$};
    \node [anchor=base west] at (12.base -| 228.west) {$\prof{=0}$};

    \node [anchor=base west] at (2282.base -| r01.west) (r02) {$\prof{\text{reste~} 0}$};
    \node [anchor=base west] at (1142.base -| r01.west) (r03) {$\prof{\text{reste~} 0}$};
    \node [anchor=base west] at (572.base -| r01.west) (r11) {$\prof{\text{reste~} 1}$};
    \node [anchor=base west] at (282.base -| r01.west) (r04) {$\prof{\text{reste~} 0}$};
    \node [anchor=base west] at (142.base -| r01.west) (r05) {$\prof{\text{reste~} 0}$};
    \node [anchor=base west] at (72.base -| r01.west) (r12) {$\prof{\text{reste~} 1}$};
    \node [anchor=base west] at (32.base -| r01.west) (r13) {$\prof{\text{reste~} 1}$};
    \node [anchor=base west] at (12.base -| r01.west) (r14) {$\prof{\text{reste~} 1}$};

    \prof{
      \draw [-stealth] (r14) -| ++(1, -1) node [anchor=north] (fleche) {$1$};
      \node [left=0 of fleche.base, anchor=base east] {$456_{(10)}=$};
      \draw [-stealth] (r13) -| ([xshift=0.25cm]fleche.north) node [anchor=north] (fleche) {$1$};
      \draw [-stealth] (r12) -| ([xshift=0.25cm]fleche.north) node [anchor=north] (fleche) {$1$};
      \draw [-stealth] (r05) -| ([xshift=0.25cm]fleche.north) node [anchor=north] (fleche) {$0$};
      \draw [-stealth] (r04) -| ([xshift=0.25cm]fleche.north) node [anchor=north] (fleche) {$0$};
      \draw [-stealth] (r11) -| ([xshift=0.25cm]fleche.north) node [anchor=north] (fleche) {$1$};
      \draw [-stealth] (r03) -| ([xshift=0.25cm]fleche.north) node [anchor=north] (fleche) {$0$};
      \draw [-stealth] (r02) -| ([xshift=0.25cm]fleche.north) node [anchor=north] (fleche) {$0$};
      \draw [-stealth] (r01) -| ([xshift=0.25cm]fleche.north) node [anchor=north] (fleche) {$0$};
      \node at ([xshift=0.05cm, yshift=0.1cm]fleche.south east) {$_{(2)}$};
    }
  \end{tikzpicture}
\end{frame}

\begin{frame}[t]{Conversion de décimal vers binaire}
  On peut aussi procéder par \emph{additions successives}.\\~\\
  Cela fonctionne bien pour les \emph{petits nombres} et est assez rapide avec un peu d'habitude, à condition de bien connaître les premières \emph{puissances de 2}.
  \vfill
  \emph{Exemple :} $76_{(10)}$
  \prof{
    \begin{itemize}
    \item $64 = 2^6$ rentre, $76-64 = 12$,
    \item $8 = 2^3$ rentre, $12-8=4$,
    \item $4 = 2^2$
    \end{itemize}
  }
  Donc $76_{(10)} = \prof{2^6 + 2^3 + 2^2 = 1001100_{(2)}}$
\end{frame}

\begin{methode}{Conversions entre bases}
  \centering
  \begin{tikzpicture}[thick, rounded corners]
    \node [draw, circle] (dec) {Décimal};
    \node [draw, circle, right=3.25 of dec] (bin) {Binaire};
    \draw [-{Latex[scale=1.5]}] (dec.north east) to [out=45, in=135] node [midway, align=center, fill=white, draw] {Divisions\\successives par 2} (bin.north west);
    \draw [-{Latex[scale=1.5]}] (bin.south west) to [out=-135, in=315] node [midway, align=center, fill=white, draw] {Somme de\\puissances de 2} (dec.south east);
  \end{tikzpicture}
  \vfill
\end{methode}

\begin{frame}{Nombre de chiffres et capacité}
  En écriture décimale et en utilisant 3 chiffres, on peut représenter les nombres de \prof{0} à \prof{999}, soit \prof{$1000 = 10^3$} nombres différents.
  \vfill
  En base $b$ en utilisant $n$ chiffres, on peut représenter $b^n$ nombres différents.
  \vfill
  Donc en base binaire avec $n$ bits on peut représenter \prof{$2^n$} nombres différents, allant de $0$ à $2^n - 1$.\\
  \emph{Exemple :} sur 10 bits, le plus grand nombre qu'on peut représenter est \prof{1023}.\\
  \emph{Exercice :} jusqu'à combien pouvez-vous compter avec vos 10 doigts ?
  \vfill
  Inversement, pour représenter un nombre $N$ en binaire il faut \prof{$\lceil\log_2(N)\rceil$} bits.
\end{frame}

\begin{exercice}{Conversions binaire - décimal}
  \emph{Exercice 1 :} Convertir en décimal les nombres binaires suivants :
  \begin{columns}
    \begin{column}{0.33\textwidth}
      \centering
      \begin{align*}
        10_{(2)} &= \prof{2_{(10)}}\\
        101101_{(2)} &= \prof{45_{(10)}}
      \end{align*}
    \end{column}
    \begin{column}{0.33\textwidth}
      \centering
      \begin{align*}
        1110_{(2)} &= \prof{14_{(10)}}\\
        1101100_{(2)} &= \prof{108_{(10)}}
      \end{align*}
    \end{column}
    \begin{column}{0.33\textwidth}
      \centering
      \begin{align*}
        1000111_{(2)} &= \prof{71_{(10)}}\\
        11101101_{(2)} &= \prof{237_{(10)}}
      \end{align*}
    \end{column}
  \end{columns}
  \vfill
  \emph{Exercice 2 :} Convertir en binaire les nombres décimaux suivants :
  \begin{columns}
    \begin{column}{0.33\textwidth}
      \centering
      \begin{align*}
        9_{(10)} & = \prof{1001_{(2)}}\\
        64_{(10)} & = \prof{100~0000_{(2)}}
      \end{align*}
    \end{column}
    \begin{column}{0.33\textwidth}
      \centering
      \begin{align*}
        13_{(10)} & = \prof{1101_{(2)}}\\
        175_{(10)} & = \prof{1010~1111_{(2)}}
      \end{align*}
    \end{column}
    \begin{column}{0.33\textwidth}
      \centering
      \begin{align*}
        58_{(10)} & = \prof{11~1010_{(2)}}\\
        255_{(10)} & = \prof{1111~1111_{(2)}}
      \end{align*}
    \end{column}
  \end{columns}
  \vfill
  \emph{Exercice 3 :} Déterminer le nombre de bits nécessaires pour coder les nombres décimaux suivants :
  \vfill
  \begin{columns}
    \begin{column}{0.33\textwidth}
      \centering
      $7_{(10)}$ : \prof{3} bits
    \end{column}
    \begin{column}{0.33\textwidth}
      \centering
      $45_{(10)}$ : \prof{6} bits
    \end{column}
    \begin{column}{0.33\textwidth}
      \centering
      $230_{(10)}$ : \prof{8} bits      
    \end{column}
  \end{columns}
  \vfill
\end{exercice}

\begin{limite}
  L'écriture binaire de grands nombres est parfois \emph{difficile à lire}.
  \vfill
  \emph{Exercice :} coder en binaire votre année de naissance.
  \vfill
  % Exemple : $10011100010000000_{(2)} = \prof{80000}_{(10)}$
  % \vfill
  On aimerait pouvoir ``regrouper'' les 0 et les 1.
\end{limite}

\subsection{Base hexadécimale}

\begin{frame}{\insertsubsection}
  \centering
  En base 16, les chiffres sont : 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, A, B, C, D, E et F.
  \vspace{-0.5cm}
  \begin{columns}[T]
    \begin{column}{0.5\textwidth}
      \begin{center}
        \begin{tabular}{ccc}
          \toprule
          Chiffre  & Quantité  & Codage binaire \\
          \midrule
          0        & 0         & 0000           \\
          1        & 1         & 0001           \\
          2        & 2         & 0010           \\
          3        & 3         & 0011           \\
          4        & 4         & 0100           \\
          5        & 5         & 0101           \\
          6        & 6         & 0110           \\
          7        & 7         & 0111           \\
          8        & 8         & 1000           \\ 
          9        & 9         & 1001           \\ 
          \bottomrule
        \end{tabular}
      \end{center}
      {\small
        Les chiffres de 0 à 9 codés en binaire forment ce qu'on appelle le code BCD (\textit{binary-coded decimal}).
      }
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{center}
        \begin{tabular}{ccc}
          \toprule
          Chiffre  & Quantité  & Codage binaire \\
          \midrule
          \emph{A} & \emph{10} & 1010           \\
          \emph{B} & \emph{11} & 1011           \\
          \emph{C} & \emph{12} & 1100           \\
          \emph{D} & \emph{13} & 1101           \\
          \emph{E} & \emph{14} & 1110           \\
          \emph{F} & \emph{15} & 1111           \\
          \bottomrule
        \end{tabular}
      \end{center}
    \end{column}
  \end{columns}
\end{frame}

\subsection{Conversions entre bases -- suite}

\begin{frame}{Conversion de binaire vers hexadécimal}
  Grouper par 4 les chiffres en binaire en commençant à droite, par les \emph{chiffres de poids faible}, et remplacer chaque bloc par un chiffre hexadécimal.
  \vfill
  \begin{center}
    \begin{tikzpicture}[thick]
      \node [inner sep=0] (nb2_split) {$1000$};
      \node [left=0 of nb2_split.west, anchor=east, inner sep=0] (nb3_split) {$0011$};
      \node [left=0 of nb3_split.west, anchor=east, inner sep=0] (nb4_split) {$\phantom{000}1$};
      \node [left=0 of nb4_split.base west, anchor=base east, inner sep=0] (80000) {$80000_{(10)} = $};
      \node [right=0 of nb2_split.east, anchor=west, inner sep=0] (nb1_split) {$1000$};
      \node [right=0 of nb1_split.east, anchor=west, inner sep=0] (nb0_split) {$0000$};
      \node [right=0 of nb0_split.base east, anchor=base west, inner sep=0] (indic) {$_{(2)}$};
      \prof{
        \draw [decoration={brace,mirror},decorate] (nb0_split.south west) -- node [below] (nb0hex) {} (nb0_split.south east);
        \draw [decoration={brace,mirror},decorate] (nb1_split.south west) -- node [below] (nb1hex) {} (nb1_split.south east);
        \draw [decoration={brace,mirror},decorate] (nb2_split.south west) -- node [below] (nb2hex) {} (nb2_split.south east);
        \draw [decoration={brace,mirror},decorate] (nb3_split.south west) -- node [below] (nb3hex) {} (nb3_split.south east);
        \draw [decoration={brace,mirror},decorate] (nb4_split.south west) -- node [below] (nb4hex) {} (nb4_split.south east);
        \node [below=-0.25 of nb0hex] {0};
        \node [below=-0.25 of nb1hex] {8};
        \node [below=-0.25 of nb2hex] {8};
        \node [below=-0.25 of nb3hex] {3};
        \node [below=-0.25 of nb4hex] {1};
      }
    \end{tikzpicture}
  \end{center}
  \vfill
  Donc $80000_{(10)} = 10011100010000000_{(2)} = \prof{13880}_{(16)}$
  \vfill
  Cela fonctionne car 16 (hexadécimal) est une puissance de 2 (binaire).
  \vfill
  C'est le même procédé lorsqu'on convertit $1789_{(10)}$ en base cent : $17\;89_{(100)}$\\
  en disant ``dix-sept cent quatre-vingt neuf'', car 100 est une puissance de 10.
\end{frame}

\begin{frame}{Conversion d'hexadécimal vers binaire}
  Remplacer chaque chiffre hexadécimal par sa représentation binaire.
  \vfill
  Exemple : $5$A$71_{(16)} = \prof{110~1010~0111~0001}_{(2)}$
\end{frame}

\begin{methode}{Conversions entre bases}
  \centering
  \begin{tikzpicture}[thick, rounded corners]
    \node [draw, circle] (dec) {Décimal};
    \node [draw, circle, right=3.25 of dec] (bin) {Binaire};
    \node [draw, circle, right=3.25 of bin, align=center] (hex) {Hexa-\\décimal};

    \draw [-{Latex[scale=1.5]}] (dec.north east) to [out=45, in=135] node [midway, align=center, fill=white, draw] {Divisions\\successives par 2} (bin.north west);
    \draw [-{Latex[scale=1.5]}] (bin.south west) to [out=-135, in=315] node [midway, align=center, fill=white, draw] {Somme de\\puissances de 2} (dec.south east);
    \draw [-{Latex[scale=1.5]}] (bin.north east) to [out=45, in=135] node [midway, align=center, fill=white, draw] {Groupement\\de 4 bits} (hex.north west);
    \draw [-{Latex[scale=1.5]}] (hex.south west) to [out=-135, in=315] node [midway, align=center, fill=white, draw] {Séparation des\\chiffres en 4 bits} (bin.south east);
  \end{tikzpicture}
  \vfill
\end{methode}

\begin{exercice}{Conversions binaire - hexadécimal}
  \emph{Exercice 1 : } Convertir en binaire les nombres hexadécimaux suivants :
  \begin{columns}
    \begin{column}{0.22\textwidth}
      \centering
      \begin{equation*}
        B_{(16)} = \prof{1010_{(2)}}
      \end{equation*}
    \end{column}
    \begin{column}{0.27\textwidth}
      \centering
      \begin{equation*}
        5C_{(16)} = \prof{101~1100_{(2)}}
      \end{equation*}
    \end{column}
    \begin{column}{0.51\textwidth}
      \centering
      \begin{equation*}
        CAFE_{(16)} = \prof{1100~1010~1111~1110_{(2)}}
      \end{equation*}
    \end{column}
  \end{columns}
  \vfill
  \emph{Exercice 2 : } Convertir en hexadécimal les nombres binaires suivants :
  \begin{columns}
    \begin{column}{0.26\textwidth}
      \centering
      \begin{equation*}
        110_{(2)} = \prof{6_{(16)}}
      \end{equation*}
    \end{column}
    \begin{column}{0.32\textwidth}
      \centering
      \begin{equation*}
        110010_{(2)} = \prof{32_{(16)}}
      \end{equation*}
    \end{column}
    \begin{column}{0.42\textwidth}
      \centering
      \begin{equation*}
        10010001101_{(2)} = \prof{48D_{(16)}}
      \end{equation*}
    \end{column}
  \end{columns}
  \vfill
\end{exercice}

\begin{limite}
  Comment convertir de décimal en hexadécimal et vice versa \emph{directement} ?
\end{limite}

\begin{frame}{Conversion d'hexadécimal vers décimal}
  Un nombre s'écrit en \emph{hexadécimal} comme une somme de puissances de \emph{16}.
  \vfill
  \centering
  \begin{tikzpicture}[thick]
    \node (A) {A};
    \node at ([xshift=0.15cm, yshift=0.1cm]A.south east) {$_{(16)}$};
    \node [left=-0.25 of A] (5) {5};
    \node [left=-0.25 of 5] (D) {D};
    \prof{
      \draw [-stealth] (A) |- ++(1, -1) node [anchor=west] (fleche) {$\prof{\text{A} \times 16^0}$};
      \draw [-stealth] (5) |- ([yshift=-0.5cm]fleche.west) node [anchor=west] {$+\prof{~5 \times 16^1}$};
      \draw [-stealth] (D) |- ([yshift=-1.0cm]fleche.west) node [anchor=west] (sommeint) {$+\prof{~\text{D} \times 16^2}$};
      \draw (sommeint.south west) -- (sommeint.south east);
      \node [below=0.65 of sommeint.west, anchor=west] (10) {$10$};
      \node [below=0.5 of 10.west, anchor=west] (80) {$+~80$};
      \node [below=0.5 of 80.west, anchor=west] (13x256) {$+~13 \times 256$};
      \draw (13x256.south west) -- (13x256.south east);
      \node [below=0.65 of 13x256.west, anchor=west] (resultat) {$3418_{(10)}$};
    }
  \end{tikzpicture}
\end{frame}

\begin{frame}{Conversion de décimal vers hexadécimal}
  De la même manière que la conversion de décimal à binaire, on effectue des \emph{divisions entières successives par la base} jusqu’à obtenir un quotient nul.
  \vfill
  Trouver l'écriture hexadécimale de $12849_{(10)}$
  \vfill
  \emph{Divisions entières successives par 16 :}
  \vfill
  \begin{tikzpicture}[thick]
    \node [] (1284916) {$\prof{12849 / 16}$};
    \node [right=1.5 of 1284916.base, anchor=base] (802) {$\prof{=802}$};
    \node [right=2.5 of 802.base, anchor=base] (rF) {$\prof{\text{reste~F}~(=15_{(10)})}$};
    \node [below=0.6 of 1284916.west, anchor=west] (80216) {$\prof{802 / 16}$};
    \node [below=0.6 of 80216.west, anchor=west] (5016) {$\prof{50 / 16}$};
    \node [below=0.6 of 5016.west, anchor=west] (316) {$\prof{3 / 16}$};

    \prof{
      \node [anchor=base west] at (80216.base -| 802.west) {$\prof{=50}$};
      \node [anchor=base west] at (5016.base -| 802.west) {$\prof{=3}$};
      \node [anchor=base west] at (316.base -| 802.west) {$\prof{=0}$};
      
      \node [anchor=base west] at (80216.base -| rF.west) (r2) {$\prof{\text{reste~} 2}$};
      \node [anchor=base west] at (5016.base -| rF.west) (r4) {$\prof{\text{reste~} 4}$};
      \node [anchor=base west] at (316.base -| rF.west) (r3) {$\prof{\text{reste~} 3}$};

      \draw [-stealth] (r3) -| ++(2, -1) node [anchor=north] (fleche) {$3$};
      \node [left=0 of fleche.base, anchor=base east] {$12849_{(10)}=$};
      \draw [-stealth] (r4) -| ([xshift=0.25cm]fleche.north) node [anchor=north] (fleche) {$4$};
      \draw [-stealth] (r2) -| ([xshift=0.25cm]fleche.north) node [anchor=north] (fleche) {$2$};
      \draw [-stealth] (rF) -| ([xshift=0.25cm]fleche.north) node [anchor=north] (fleche) {F};
      \node at ([xshift=0.05cm, yshift=0.1cm]fleche.south east) {$_{(16)}$};
    }
  \end{tikzpicture}
  \vfill
  Donc $12849_{(10)} = \prof{342\text{F}}_{(16)}$
  \vfill
\end{frame}

\begin{methode}{Conversions entre bases}
  \centering
  \begin{tikzpicture}[thick, rounded corners]
    \node [draw, circle] (dec) {Décimal};
    \node [draw, circle, right=3.25 of dec] (bin) {Binaire};
    \node [draw, circle, right=3.25 of bin, align=center] (hex) {Hexa-\\décimal};

    \draw [-{Latex[scale=1.5]}] (dec.north east) to [out=45, in=180-45] node [midway, align=center, fill=white, draw] {Divisions\\successives par 2} (bin.north west);
    \draw [-{Latex[scale=1.5]}] (bin.south west) to [out=-180+45, in=-45] node [midway, align=center, fill=white, draw] {Somme de\\puissances de 2} (dec.south east);
    \draw [-{Latex[scale=1.5]}] (bin.north east) to [out=45, in=180-45] node [midway, align=center, fill=white, draw] {Groupement\\de 4 bits} (hex.north west);
    \draw [-{Latex[scale=1.5]}] (hex.south west) to [out=-180+45, in=-45] node [midway, align=center, fill=white, draw] {Séparation des\\chiffres en 4 bits} (bin.south east);
    \draw [-{Latex[scale=1.5]}] (dec.north) to [out=90, in=90, looseness=0.75] node [midway, align=center, fill=white, draw] {Divisions\\successives par 16} (hex.north);
    \draw [-{Latex[scale=1.5]}] (hex.south) to [out=270, in=270, looseness=0.75] node [midway, align=center, fill=white, draw] {Somme de\\puissances de 16} (dec.south);
  \end{tikzpicture}
\end{methode}

\begin{exercice}{Conversions décimal - hexadécimal}
  \textbf{Exercice 1 :} Convertir en décimal les nombres hexadécimaux suivants :
  \begin{columns}
    \begin{column}{0.33\textwidth}
      \centering
      \begin{equation*}
        9_{(16)} = \prof{9_{(10)}}
      \end{equation*}
    \end{column}
    \begin{column}{0.33\textwidth}
      \centering
      \begin{equation*}
        85_{(16)} = \prof{133_{(10)}}
      \end{equation*}
    \end{column}
    \begin{column}{0.33\textwidth}
      \centering
      \begin{equation*}
        1000_{(16)} = \prof{4096_{(10)}}
      \end{equation*}
    \end{column}
  \end{columns}
  \vfill
  \textbf{Exercice 2 :} Convertir en hexadécimal les nombres décimaux suivants :
  \begin{columns}
    \begin{column}{0.33\textwidth}
      \centering
      \begin{equation*}
        14_{(10)} = \prof{E_{(16)}}
      \end{equation*}      
    \end{column}
    \begin{column}{0.33\textwidth}
      \centering
      \begin{equation*}
        192_{(10)} = \prof{C0_{(16)}}
      \end{equation*}      
    \end{column}
    \begin{column}{0.33\textwidth}
      \centering
      \begin{equation*}
        8228_{(10)} = \prof{2024_{(16)}}
      \end{equation*}      
    \end{column}
  \end{columns}
\end{exercice}

\subsection{Bilan du chapitre}

\begin{frame}{\insertsubsection}
  Vous êtes à présent capables de :
  \begin{itemize}
  \vfill\item Représenter un entier naturel en base binaire,
  \vfill\item Représenter un entier naturel en base hexadécimale,
  \vfill\item Convertir directement entre les bases binaire, décimale et hexadécimale.
  \end{itemize}
\end{frame}

\begin{limite}
  Peut-on concevoir des circuits électroniques numériques\\
  qui manipulent des nombres ?
  \vfill
  Peut-on concevoir des circuits numériques pour réaliser\\
  des \emph{opérations arithmétiques} en binaire ?
  \vfill
  Comment fabriquer une \emph{calculatrice} ?\\~\\
  (interdite à l'examen tant qu'on ne sait pas faire)
\end{limite}
