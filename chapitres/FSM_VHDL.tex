\section{Machine d'états en VHDL}

\subsection{Architecture matérielle d'une machine d'états}

\begin{frame}{\insertsubsection}
  \begin{enumerate}
  \item des \textcolor{rbw1}{\emph{états}} possibles,
  \item des \textcolor{rbw2}{\emph{transisitions}} possibles entre certains états,
  \item des \textcolor{rbw4}{\emph{conditions}} sous lesquelles la transition entre deux états a lieu,
  \item des \textcolor{rbw5}{\emph{valeurs de sortie}} associées à chaque état.
  \end{enumerate}
  \vfill
  \centering
  \begin{tikzpicture}[thick]
    \node [flipflop DFFrstn, fill=rbw1, ultra thick, scale=0.8] (state) {};
    \node [combinatoire, fill=rbw5, right=0.5 of state.pin 6, font=\footnotesize] (calcul_sorties) {Calcul\\sorties};
    \draw (state.pin 6) to [multiwire=n] (calcul_sorties.west);
    \node [combinatoire,
    path picture={%
      \fill[rbw2] (path picture bounding box.south west)
      -- (path picture bounding box.north east) |-cycle;
      \fill[rbw4] (path picture bounding box.north east)
      -- (path picture bounding box.south west) |-cycle;},
    left=0.5 of state.pin 1, font=\footnotesize] (calcul_etat_suivant) {Calcul\\état suivant};
    \draw (calcul_etat_suivant.east) to [multiwire=n] (state.pin 1);
    \draw [latex-] (calcul_etat_suivant.180) to ++(-0.5, 0) node [anchor=south east] (entrees) {entrées};
    \draw [-latex] (state.pin 6) to ++(0, 1.5) coordinate (up) -| (calcul_etat_suivant.90);
    \draw [-latex] (calcul_sorties.east) to ++(0.5, 0) node [anchor=south west] (sorties) {sorties};

    \draw (state.pin 3) to (state.pin 3 -| entrees.south east) node [anchor=south east] (clk) {\texttt{clk}};
    \draw (state.down) to (state.down -| entrees.south east) node [anchor=south east] {\texttt{rst\_n}};

    \draw ([yshift=0.5cm]up -| clk.east) rectangle ([yshift=-0.5cm]state.down -| sorties.west);
  \end{tikzpicture}
\end{frame}

\begin{frame}{\insertsubsection}
  \begin{center}
    \begin{tikzpicture}[thick]
      \node [flipflop DFFrstn, fill=rbw1, ultra thick, scale=0.8] (state) {};
      \node [combinatoire, fill=rbw5, right=0.5 of state.pin 6, font=\footnotesize] (calcul_sorties) {Calcul\\sorties};
      \draw (state.pin 6) to [multiwire=n] (calcul_sorties.west);
      \node [combinatoire,
      path picture={%
        \fill[rbw2] (path picture bounding box.south west)
        -- (path picture bounding box.north east) |-cycle;
        \fill[rbw4] (path picture bounding box.north east)
        -- (path picture bounding box.south west) |-cycle;},
      left=0.5 of state.pin 1, font=\footnotesize] (calcul_etat_suivant) {Calcul\\état suivant};
      \draw (calcul_etat_suivant.east) to [multiwire=n] (state.pin 1);
      \draw [latex-] (calcul_etat_suivant.180) to ++(-0.5, 0) node [anchor=south east] (entrees) {entrées};
      \draw [-latex] (state.pin 6) to ++(0, 1.5) coordinate (up) -| (calcul_etat_suivant.90);
      \draw [-latex] (calcul_sorties.east) to ++(0.5, 0) node [anchor=south west] (sorties) {sorties};

      \draw (state.pin 3) to (state.pin 3 -| entrees.south east) node [anchor=south east] (clk) {\texttt{clk}};
      \draw (state.down) to (state.down -| entrees.south east) node [anchor=south east] {\texttt{rst\_n}};

      \draw ([yshift=0.5cm]up -| clk.east) rectangle ([yshift=-0.5cm]state.down -| sorties.west);
    \end{tikzpicture}
  \end{center}
  \vfill
  \begin{itemize}
  \item Le \textcolor{rbw1}{\emph{registre d'état}} stocke l'état \emph{actuel}, sur $n$ bits
  \item La \textcolor{rbw4}{\emph{logique combinatoire de}} \textcolor{rbw2}{\emph{calcul de l'état suivant}} détermine l'état \emph{suivant} à partir de l'état \emph{actuel} et des \emph{entrées},
  \item La \textcolor{rbw5}{\emph{logique combinatoire de calcul des sorties}} calcule les sorties à partir de l'état \emph{actuel}.
  \item Le signal \texttt{rst\_n} place la machine d'états dans son \emph{état initial},
  \item Le signal \texttt{clk} commande la \emph{mise à jour de l'état actuel}.
  \end{itemize}
\end{frame}

\subsection{Contraintes temporelles pour le fonctionnement des bascules}

\begin{frame}{\insertsubsection}
  Pour qu'une bascule D fonctionne correctement, deux \emph{contraintes temporelles} doivent être respectées :
  \begin{itemize}
  \item $t_\text{setup}$ : durée pendant laquelle la donnée doit être présente sur l'entrée D \emph{avant} le front montant de l'horloge.
  \item $t_\text{hold}$ : durée pendant laquelle la donnée doit être présente sur l'entrée D \emph{après} le front montant de l'horloge.
  \end{itemize}
  \vfill
  \centering
  \begin{tikztimingtable}[
    ultra thick,
    xscale=30,
    yscale=1.5,
    timing/slope=0,
    timing/rowdist=0.5cm,
    timing/coldist=0.5pt,
    ]
    \texttt{clk} & 0.6L1{c}0.1L \\
    $D$          & 0.45L0.25H0.5L \\
    $Q$          & 0.8L0.2H0.2L \\
    \extracode
    \begin{pgfonlayer}{background}
      \begin{scope}[thick,gray]
        \horlines{}
        \vertlines[dashed] {0.1,1.1};
        \draw [latex-latex, ultra thick, red] (0.45,-1.25) -- node [midway, below] {$t_\text{setup}$} ++(0.15,0);
        \draw [latex-latex, ultra thick, vert] (0.6,-1.25) -- node [midway, below] {$t_\text{hold}$}++(0.1,0);
        \draw [-latex, ultra thick, main] (0.6, 0.5) to ++(0.05, 0) to [out=0, in=160, looseness=0.12] node [midway, right] {$t_\text{clk\_to\_Q}$} ++(0.15, -3.5);
      \end{scope}
    \end{pgfonlayer}
  \end{tikztimingtable}%
  \vfill
  Ces valeurs sont données par le \emph{concepteur / fabriquant} de la bascule D.
\end{frame}

\begin{frame}{Contraintes temporelles pour l'implantation d'une FSM}
  Pour éviter les erreurs sur le registre d'état, il faut \emph{synchroniser les entrées}.
  \vfill
  \centering
  \begin{tikzpicture}[thick]
    \node [flipflop DFFrstn, fill=rbw1, ultra thick, scale=0.8] (state) {};
    \node [combinatoire, fill=rbw5, right=0.3 of state.pin 6, font=\footnotesize] (calcul_sorties) {Calcul\\sorties};
    \draw (state.pin 6) to [multiwire=n] (calcul_sorties.west);
    \node [combinatoire,
    path picture={%
      \fill[rbw2] (path picture bounding box.south west)
      -- (path picture bounding box.north east) |-cycle;
      \fill[rbw4] (path picture bounding box.north east)
      -- (path picture bounding box.south west) |-cycle;},
    left=0.3 of state.pin 1, font=\footnotesize] (calcul_etat_suivant) {Calcul\\état suivant};
    \draw (calcul_etat_suivant.east) to [multiwire=n] (state.pin 1);
    \node [flipflop DFFrstn, fill=rbw8, ultra thick, scale=0.8, left=0 of calcul_etat_suivant.west, anchor=pin 6] (sync) {};
    \draw [latex-] (sync.pin 1) to ++(-0.35, 0) node [anchor=south east] (entrees) {entrées};
    \draw [-latex] (state.pin 6) to ++(0, 1.5) coordinate (up) -| (calcul_etat_suivant.90);
    \draw [-latex] (calcul_sorties.east) to ++(0.35, 0) node [anchor=south west] (sorties) {sorties};

    \draw (state.pin 3) to ++(0, -0.75) coordinate (clk) to (clk -| entrees.south east) node [anchor=south east] (clk) {\texttt{clk}};
    \draw (clk.south east) to (clk.south east -| sync.pin 3) to [short, *-] (sync.pin 3);
    \draw (state.down) to ++(0, -0.75) coordinate (rst) to (rst -| entrees.south east) node [anchor=south east] (rst) {\texttt{rst\_n}};
    \draw (rst.south east) to (rst.south east -| sync.down) to [short, *-] (sync.down);

    \draw ([yshift=0.5cm]up -| rst.east) rectangle ([yshift=-0.5cm]rst.south -| sorties.west);
  \end{tikzpicture}
\end{frame}

\subsection{Implantation d'une machine d'états en VHDL}

\begin{frame}{Entité}
  \inputminted[lastline=14, fontsize=\small]{vhdl}{vhdl/bike_ctrl.vhd}
\end{frame}

\begin{frame}{Type énuméré pour le codage des états}
  On utilise un \emph{type énuméré} pour coder les états :\\
  \vhdl{TYPE nom_du_type IS (valeur_1, valeur_2, ..., valeur_n);}
  \vfill
  Puis on déclare le signal qui aura ce nouveau type :\\
  \vhdl{SIGNAL nom_du_signal : nom_du_type;}
  \vfill
  \inputminted[firstline=16, lastline=26, highlightlines={18,19}, fontsize=\small]{vhdl}{vhdl/bike_ctrl.vhd}
\end{frame}

\begin{frame}{Mise à jour de l'état et calcul de l'état suivant}
  On utilise une structure \vhdl{CASE} pour décrire toutes les \emph{transitions} possibles, avec leurs \emph{conditions associées}.
  \begin{columns}[T]
    \begin{column}{0.5\textwidth}
      \inputminted[firstline=28, lastline=46, highlightlines={36,37,41}, fontsize=\scriptsize]{vhdl}{vhdl/bike_ctrl.vhd}
    \end{column}
    \begin{column}{0.5\textwidth}
      \inputminted[firstline=47, lastline=65, highlightlines={47,55,61,63}, fontsize=\scriptsize]{vhdl}{vhdl/bike_ctrl.vhd}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Calcul des sorties}
  Une structure \vhdl{WITH ... SELECT} donne les valeurs des sorties pour chaque état possible.
  \vfill
  \inputminted[firstline=67]{vhdl}{vhdl/bike_ctrl.vhd}
  \vfill
  On aurait aussi pu utiliser une structure \vhdl{WHEN ... ELSE}.
\end{frame}

