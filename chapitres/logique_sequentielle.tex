\section{Logique séquentielle}

\subsection{Définitions}

\begin{frame}{Logique combinatoire VS séquentielle}
  Système \emph{combinatoire}\\~\\
  Les \emph{sorties} du système dépendent :
  \begin{itemize}
  \item \prof{des \emph{entrées}}.
  \end{itemize}
  \vfill
  \emph{Exemples} :
  \begin{itemize}
  \item Sur une télécommande :
    \begin{itemize}
    \item 1, 2, 3, 4, ..., 9
    \end{itemize}
  \item Boîte de vitesse manuelle.
  \end{itemize}
  \vfill
  \emph{Composants matériels} :
  \begin{itemize}
  \item Portes logiques
  \end{itemize}
  \centering
  \begin{tikzpicture}[thick]
    \node [combinatoire] (comb) at (0,0) {Logique\\combinatoire};
    \prof{\node [left=1 of comb, anchor=south east] (entrees) {Entrées};
    \draw [-latex] (entrees.south east) -- (comb.west);
    \node [right=1 of comb, anchor=south west] (sorties) {Sorties};
    \draw [-latex] (comb.east) -- (sorties.south west);}
  \end{tikzpicture}
\end{frame}

\begin{frame}{Logique combinatoire VS séquentielle}
  \begin{columns}
    \begin{column}{0.49\textwidth}
      Système \emph{séquentiel}\\~\\
      Les \emph{sorties} du système dépendent :
      \begin{itemize}
      \item de l'état \prof{\emph{actuel} du système}.
      \end{itemize}
      L'état \emph{suivant} du système dépend :
      \begin{itemize}
      \item des \prof{\emph{entrées}},
      \item de l'état \prof{\emph{actuel}} du système.
      \end{itemize}
    \end{column}
    \begin{column}{0.49\textwidth}
      \emph{Exemples} :
      \begin{itemize}
      \item Sur une télécommande :
        \begin{itemize}
        \item \faFastBackward~~\faFastForward
        \end{itemize}
      \item Palettes au volant.
      \end{itemize}
      \emph{Composants matériels} :
      \begin{itemize}
      \item Portes logiques,
      \item Éléments de \emph{mémorisation}.
      \end{itemize}
    \end{column}
  \end{columns}
  \vfill
  \begin{center}
    \begin{tikzpicture}[thick]
      \node [combinatoire] (comb_in_state) {Logique\\combinatoire};
      \node [combinatoire, right=2 of comb_in_state] (comb_state_out) {Logique\\combinatoire};
      \node [left=1 of comb_in_state, anchor=south] (entrees) {Entrées};
      \draw [-latex] (entrees.south) -- (comb.west);
      \node [right=1 of comb_state_out, anchor=south] (sorties) {Sorties};
      \draw [-latex] (comb_state_out.east) -- (sorties.south);
      \node [draw, align=center] at ($(comb_in_state)!0.5!(comb_state_out)$) (etat) {État\\actuel};
      \draw [-latex] (comb_in_state) -- (etat);
      \draw [-latex] (etat) -- (comb_state_out);
      \draw [-latex] (etat.south) -- ++(0, -1) coordinate (down) -- (down -| entrees) -- (entrees |- comb_in_state.200) -- (comb_in_state.200);
    \end{tikzpicture}
  \end{center}
  \vfill
  On peut ensuite \emph{chaîner} ces éléments pour créer un système plus complexe.
\end{frame}

\subsection{Éléments de mémorisation}

\begin{frame}{Élément de mémorisation basique}
  On veut un circuit \emph{stable}, aussi \emph{simple} que possible, \emph{stockant} une valeur logique donnée sans avoir besoin de la \emph{mainteni}r par un moyen extérieur.
  \vfill
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \centering
      Idée initiale\\~\\
      \begin{tikzpicture}[thick]
        \node [ieeestd not port] (GD) {};
        \node [ieeestd not port, rotate=180, below=2 of GD] (DG) {};
        \draw (GD.out) -- node [midway, anchor=west] {\texttt{0}} (DG.in);
        \draw (DG.out) -- node [midway, anchor=east] {\texttt{1}} (GD.in);
      \end{tikzpicture}
    \end{column}
    \begin{column}{0.5\textwidth}
      \centering
      Version plus lisible\\~\\
      \begin{circuitikz}[thick]
        \node [ieeestd not port] (haut) {};
        \node [ieeestd not port, below=1 of haut] (bas) {};
        \draw (haut.out) --++(0, -0.5) -- ([yshift=0.5cm]bas.in) -- (bas.in);
        \draw (bas.out) --++(0, 0.5) -- ([yshift=-0.5cm]haut.in) -- (haut.in);
        \draw (haut.out) to [short, *-] ++(0.5, 0) node [anchor=south west] {$Q$};
        \draw (bas.out) to [short, *-] ++(0.5, 0) node [anchor=south west] {$\overline{Q}$};
      \end{circuitikz}
    \end{column}
  \end{columns}
\end{frame}

\begin{limite}
  La valeur est bien \emph{mémorisée}, mais on ne peut pas la \emph{choisir}.
  \vfill
  Le circuit n'a \emph{pas d'entrée} pour fixer la valeur mémorisée.
\end{limite}

\begin{frame}{Élément de mémorisation avec entrées de mise à 0/1}
  \begin{columns}
    \begin{column}{0.4\textwidth}
      \renewcommand{\arraystretch}{1.25}
      \centering
      Table de vérité souhaitée                          \\~\\
      \begin{tabular}{|c|c||c|c|}
        \hline
        $R$  & $S$  & $Q$   & $\overline{Q}$   \\
        \hline
        0    & 0    & $Q^-$ & $\overline{Q}^-$ \\
        \hline
        \one & 0    & 0     & \one             \\
        \hline
        0    & \one & \one  & 0                \\
        \hline
      \end{tabular}
      \vspace{0.5cm}
      \begin{itemize}
      \item $R$ : reset (mise à 0),
      \item $S$ : set (mise à 1),
      \item $Q^-$ : valeur \emph{précédente} de $Q$.
      \end{itemize}
    \end{column}
    \begin{column}{0.45\textwidth}
      \centering
      \begin{circuitikz}[thick]
        \node [fill=gray, minimum size=1.25cm] (haut) {};
        \node [fill=gray, minimum size=1.25cm, below=1 of haut] (bas) {};
        \draw (haut.east) --++(0.5, 0) --++(0, -0.5) -- ([xshift=-0.5cm, yshift=0.5cm]bas.160) -- ++(0, -0.5) -- (bas.160);
        \draw (bas.east) --++(0.5, 0) --++(0, 0.5) -- ([xshift=-0.5cm, yshift=-0.5cm]haut.200) -- ++(0, 0.5) -- (haut.200);
        \draw (haut.east) --++(0.5, 0) to [short, *-] ++(0.5, 0) node [anchor=south west] {$Q$};
        \draw (bas.east) --++(0.5, 0) to [short, *-] ++(0.5, 0) node [anchor=south west] {$\overline{Q}$};
        \draw (haut.160) --++(-0.5, 0) node [anchor=south east] {$S$};
        \draw (bas.200) --++(-0.5, 0) node [anchor=south east] {$R$};
      \end{circuitikz}
                                             \\~\\
      \renewcommand{\arraystretch}{1.25}
      \begin{tabular}{|c|c||c|}
        \hline
        $S$  & $\overline{Q}$ & $Q$           \\
        \hline
        0    & 0             & 1             \\
        \hline
        0    & \one          & 0             \\
        \hline
        \one & 0             & 1             \\
        \hline
        \one & \one          & 1             \\
        \hline
      \end{tabular}
      \hfill
      \begin{tabular}{|c|c||c|}
        \hline
        $R$  & $Q$           & $\overline{Q}$ \\
        \hline
        0    & 0             & 1             \\
        \hline
        0    & \one          & 0             \\
        \hline
        \one & 0             & 1             \\
        \hline
        \one & \one          & 1             \\
        \hline
      \end{tabular}
    \end{column}    
  \end{columns}
\end{frame}

\begin{frame}{Fonction logique mystère}
  \begin{columns}
    \begin{column}{0.4\textwidth}
      \centering
      \renewcommand{\arraystretch}{1.25}
      \begin{tabular}{|c|c||c|}
        \hline
        $S$ / $R$                       & $\overline{Q}$ & $Q$ \\
        \hline
        0                               & 0              & 1   \\
        \hline
        0                               & \one           & 0   \\
        \hline
        \one                            & 0              & 1   \\
        \hline
        \one                            & \one           & 1   \\
        \hline
      \end{tabular}
                                                               \\~\\~\\
      {\Large\faArrowDown}\\~\\
      \begin{tabular}{|c|c||c|}
        \hline
        $\overline{S}$ / $\overline{R}$ & $\overline{Q}$ & $Q$ \\
        \hline
        0                               & 0              & \prof{1}   \\
        \hline
        0                               & \one           & \prof{1}   \\
        \hline
        \one                            & 0              & \prof{1}   \\
        \hline
        \one                            & \one           & \prof{0}   \\
        \hline
      \end{tabular}
      \\~\\
      Fonction \prof{\emph{NAND}}
    \end{column}
    \prof{%
      \begin{column}{0.6\textwidth}
        \centering
        \begin{circuitikz}[thick]
          \node [ieeestd nand port] (haut) {};
          \node [ieeestd nand port, below=1 of haut] (bas) {};
          \draw (haut.out) --++(0, -0.5) -- ([yshift=0.5cm]bas.in 1) -- (bas.in 1);
          \draw (bas.out) --++(0, 0.5) -- ([yshift=-0.5cm]haut.in 2) -- (haut.in 2);
          \draw (haut.out) to [short, *-] ++(0.5, 0) node [anchor=south west] {$Q$};
          \draw (bas.out) to [short, *-] ++(0.5, 0) node [anchor=south west] {$\overline{Q}$};
          \draw (haut.in 1) --++(-0.5, 0) node [anchor=south east] {$\overline{S}$};
          \draw (bas.in 2) --++(-0.5, 0) node [anchor=south east] {$\overline{R}$};
        \end{circuitikz}
        \\~\\
        \begin{circuitikz}[thick]
          \node [ieeestd nand port] (haut) {};
          \node [ieeestd nand port, below=1 of haut] (bas) {};
          \draw (haut.out) --++(0, -0.5) -- ([yshift=0.5cm]bas.in 1) -- (bas.in 1);
          \draw (bas.out) --++(0, 0.5) -- ([yshift=-0.5cm]haut.in 2) -- (haut.in 2);
          \draw (haut.out) to [short, *-] ++(0.5, 0) node [anchor=south west] {$Q$};
          \draw (bas.out) to [short, *-] ++(0.5, 0) node [anchor=south west] {$\overline{Q}$};
          \node [ieeestd nand port, left=0 of haut.in 1] (invS) {};
          \node [ieeestd nand port, left=0 of bas.in 2] (invR) {};
          \draw (invS.in 1) to node [midway, inner sep=0] (Sbar) {} (invS.in 2);
          \draw (invR.in 1) to node [midway, inner sep=0] (Rbar) {} (invR.in 2);
          \draw (Sbar) to [short, *-] ++(-0.5, 0) node [anchor=south east] {$S$};
          \draw (Rbar) to [short, *-] ++(-0.5, 0) node [anchor=south east] {$R$};
          \node [anchor=south] at (invS.out) {$\overline{S}$};
          \node [anchor=south] at (invR.out) {$\overline{R}$};
        \end{circuitikz}
      \end{column}
    }
  \end{columns}
\end{frame}

\begin{frame}{Verrou RS}
  \centering
  \renewcommand{\arraystretch}{1.25}
  \begin{tabular}{|c|c||c|c|l}
    \cline{1-4}
    $R$ & $S$ & $Q$   & $\overline{Q}$   &                                  \\
    \cline{1-4}
    0   & 0   & $Q^-$ & $\overline{Q}^-$ & Mémorisation de l'état précédent \\
    \cline{1-4}
    1   & 0   & 0     & 1                & Forçage à 1                      \\
    \cline{1-4}
    0   & 1   & 1     & 0                & Forçage à 0                      \\
    \cline{1-4}
  \end{tabular}
  \vfill
  \begin{circuitikz}[thick]
    \node [ieeestd nand port] (haut) {};
    \node [ieeestd nand port, below=1 of haut] (bas) {};
    \draw (haut.out) --++(0, -0.5) -- ([yshift=0.5cm]bas.in 1) -- (bas.in 1);
    \draw (bas.out) --++(0, 0.5) -- ([yshift=-0.5cm]haut.in 2) -- (haut.in 2);
    \draw (haut.out) to [short, *-] ++(0.5, 0) node [anchor=south west] {$Q$};
    \draw (bas.out) to [short, *-] ++(0.5, 0) node [anchor=south west] {$\overline{Q}$};
    \node [ieeestd nand port, left=0 of haut.in 1] (invS) {};
    \node [ieeestd nand port, left=0 of bas.in 2] (invR) {};
    \draw (invS.in 1) to node [midway, inner sep=0] (Sbar) {} (invS.in 2);
    \draw (invR.in 1) to node [midway, inner sep=0] (Rbar) {} (invR.in 2);
    \draw (Sbar) to [short, *-] ++(-0.5, 0) node [anchor=south east] {$S$};
    \draw (Rbar) to [short, *-] ++(-0.5, 0) node [anchor=south east] {$R$};
  \end{circuitikz}    
\end{frame}

\begin{limite}
  Le verrou RS possède un \emph{état ambigu}, si $R=1$ ET $S=1$.
  \vfill
  \begin{circuitikz}[thick]
    \node [ieeestd nand port] (haut) {};
    \node [ieeestd nand port, below=1 of haut] (bas) {};
    \draw (haut.out) --++(0, -0.5) -- ([yshift=0.5cm]bas.in 1) -- (bas.in 1);
    \draw (bas.out) --++(0, 0.5) -- ([yshift=-0.5cm]haut.in 2) -- (haut.in 2);
    \draw (haut.out) to [short, *-] ++(0.5, 0) node [anchor=south west] {$Q = \prof{1}$};
    \draw (bas.out) to [short, *-] ++(0.5, 0) node [anchor=south west] {$\overline{Q} = \prof{1}$};
    \node [ieeestd nand port, left=0.75 of haut.in 1] (invS) {};
    \node [ieeestd nand port, left=0.75 of bas.in 2] (invR) {};
    \draw (invS.out) -- (haut.in 1);
    \draw (invR.out) -- (bas.in 2);
    \draw (invS.in 1) to node [midway, inner sep=0] (Sbar) {} (invS.in 2);
    \draw (invR.in 1) to node [midway, inner sep=0] (Rbar) {} (invR.in 2);
    \draw (Sbar) to [short, *-] ++(-0.5, 0) node [anchor=south east] {$S=\prof{1}$};
    \draw (Rbar) to [short, *-] ++(-0.5, 0) node [anchor=south east] {$R=\prof{1}$};
    \node [anchor=south west] at (invS.bout) {$\overline{S} = \prof{0}$};
    \node [anchor=south west] at (invR.bout) {$\overline{R} = \prof{0}$};
  \end{circuitikz}
  \vfill
  \emph{Problème} : \prof{$Q = \overline{Q}$} !
\end{limite}

\begin{frame}{Verrou RS à autorisation}
  On ajoute une entrée pour \emph{autoriser} la prise en compte des signaux $R$ et $S$.
  \vfill
  \begin{columns}
    \begin{column}{0.4\textwidth}
      \renewcommand{\arraystretch}{1.25}
      \begin{tabular}{|c|c|c||c|c|l}
        \hline
        $ena$ & $S$  & $R$  & $Q$   & $\overline{Q}$           \\
        \hline
        0     & 0    & 0    & \prof{$Q^-$} & \prof{$\overline{Q}^-$}         \\
        \hline
        0     & 0    & \one & \prof{$Q^-$} & \prof{$\overline{Q}^-$}         \\
        \hline
        0     & \one & 0    & \prof{$Q^-$} & \prof{$\overline{Q}^-$}         \\
        \hline
        0     & \one & \one & \prof{$Q^-$} & \prof{$\overline{Q}^-$}         \\
        \hline
        \one  & 0    & 0    & \prof{$Q^-$} & \prof{$\overline{Q}^-$}         \\
        \hline
        \one  & 0    & \one & \prof{1}     & \prof{0}                        \\
        \hline
        \one  & \one & 0    & \prof{0}     & \prof{1}                        \\
        \hline
        \one  & \one & \one & \multicolumn{2}{c|}{\prof{\emph{Ambiguïté !}}} \\
        \hline
      \end{tabular}
    \end{column}
    \begin{column}{0.55\textwidth}
      \begin{circuitikz}[thick]
        \node [ieeestd nand port] (haut) {};
        \node [ieeestd nand port, below=1 of haut] (bas) {};
        \draw (haut.out) --++(0, -0.5) -- ([yshift=0.5cm]bas.in 1) -- (bas.in 1);
        \draw (bas.out) --++(0, 0.5) -- ([yshift=-0.5cm]haut.in 2) -- (haut.in 2);
        \draw (haut.out) to [short, *-] ++(0.5, 0) node [anchor=south west] {$Q$};
        \draw (bas.out) to [short, *-] ++(0.5, 0) node [anchor=south west] {$\overline{Q}$};
        \node [ieeestd nand port, left=0 of haut.in 1] (invS) {};
        \node [ieeestd nand port, left=0 of bas.in 2] (invR) {};
        \node [anchor=south east] at (invS.in 1) {$S$};
        \node [anchor=south east] at (invR.in 2) {$R$};
        \draw (invS.in 2) to node [midway, inner sep=0] (ena) {} (invR.in 1);
        \draw (ena) to [short, *-] ++(-0.5, 0) node [anchor=south east] {$ena$};
      \end{circuitikz} 
    \end{column}
  \end{columns}
\end{frame}

\begin{limite}
  Il y a \emph{toujours un état ambigu} ($ena=1$, $R=1$, $S=1$)...
  \vfill
  On peut faire en sorte que la condition $R \neq S$ soit toujours vraie\footnote{La condition $R=S$ n'a pas de sens : on ne force jamais à 0 et 1 simultanément...} !
\end{limite}

\begin{frame}{Verrou D à autorisation}
  \centering
  \begin{columns}
    \begin{column}{0.78\textwidth}
      \begin{circuitikz}[thick]
        \node [ieeestd nand port] (haut) {};
        \node [ieeestd nand port, below=1 of haut] (bas) {};
        \draw (haut.out) --++(0, -0.5) -- ([yshift=0.5cm]bas.in 1) -- (bas.in 1);
        \draw (bas.out) --++(0, 0.5) -- ([yshift=-0.5cm]haut.in 2) -- (haut.in 2);
        \draw (haut.out) to [short, *-] ++(0.5, 0) node [anchor=south west] {$Q$};
        \draw (bas.out) to [short, *-] ++(0.5, 0) node [anchor=south west] {$\overline{Q}$};
        \node [ieeestd nand port, left=0 of haut.in 1] (invS) {};
        \node [ieeestd nand port, left=0 of bas.in 2] (invR) {};
        \node [ieeestd nand port, left=0 of invR.in 2] (invD) {};
        \node [left=3 of invS.in 1, anchor=south east] (D) {$D$};
        % \draw (D.south east) -- (invS.in 1);
        \draw (D.south east) to (D.south east -| invD.in 1) coordinate (Dtmp) to [short, *-] (invD.in 1) to [short, *-] (invD.in 2);
        \draw (Dtmp) -- (invS.in 1);
        \draw (invS.in 2) to node [midway, inner sep=0] (ena) {} (invR.in 1);
        \draw (ena) to [short, *-] (ena -| D.south east) node [anchor=south east] {$ena$};
      \end{circuitikz}
    \end{column}
    \begin{column}{0.21\textwidth}
      \begin{tikzpicture}[thick]
        \node [flipflop Dlatch] {};
      \end{tikzpicture}
    \end{column}
  \end{columns}
  \vfill
  \begin{columns}
    \begin{column}{0.33\textwidth}
      \centering
      Table de vérité                                   \\~\\
      \renewcommand{\arraystretch}{1.25}
      \begin{tabular}{|c|c||c|c|}
        \hline
        $ena$          & $D$ & $Q$   & $\overline{Q}$   \\
        \hline
        0              & x   & \prof{$Q^-$} & \prof{$\overline{Q}^-$} \\
        \hline
        \one           & x   & \prof{$D$}   & \prof{$\overline{D}$}   \\
        \hline
      \end{tabular}
    \end{column}
    \begin{column}{0.65\textwidth}
      \centering
      Chronogramme                                      \\\vspace{0.1cm}
      \profetudiant{%
        \begin{tikztimingtable}[
          ultra thick,
          xscale=3,
          yscale=1.25,
          timing/slope=0,
          timing/rowdist=0.5cm,
          timing/c/no arrows
          ]
          $ena$          & 2{LLHH}                        \\
          $D$            & 0.6L 0.2H 2L 2H 1.2L 2L        \\
          $Q$            & 2.8L         3.2H    2L        \\
          $\overline{Q}$ & 2.8H         3.2L    2H        \\
          \extracode
          \begin{pgfonlayer}{background}
            \begin{scope}[thick,gray]
              \horlines{}
              \vertlines[dashed] {};
              \node [fill=white] at (1, 2) {\textcolor{red}{verrouillée}};
              \node [fill=white] at (3, 2) {\textcolor{vert}{passante}};
              \node [fill=white] at (5, 2) {\textcolor{red}{verrouillée}};
              \node [fill=white] at (7, 2) {\textcolor{vert}{passante}};
              \draw [fill=red!50, opacity=0.5] (0, 1.5) rectangle (2, -6);
              \draw [fill=red!50, opacity=0.5] (4, 1.5) rectangle (6, -6);
              \draw [fill=vert, opacity=0.5] (2, 1.5) rectangle (4, -6);
              \draw [fill=vert, opacity=0.5] (6, 1.5) rectangle (8, -6);
            \end{scope}
          \end{pgfonlayer}
        \end{tikztimingtable}%
      }{\begin{tikztimingtable}[
          ultra thick,
          xscale=3,
          yscale=1.25,
          timing/slope=0,
          timing/rowdist=0.5cm,
          timing/c/no arrows
          ]
          $ena$          & 2{LLHH}                        \\
          $D$            & 0.6L 0.2H 2L 2H 1.2L 2L        \\
          $Q$            & \\
          $\overline{Q}$ & \\
          \extracode
          \begin{pgfonlayer}{background}
            \begin{scope}[thick]
              \vertlines[gray,dashed] {};
              \node [fill=white] at (1, 2) {\textcolor{red}{verrouillée}};
              \node [fill=white] at (3, 2) {\textcolor{vert}{passante}};
              \node [fill=white] at (5, 2) {\textcolor{red}{verrouillée}};
              \node [fill=white] at (7, 2) {\textcolor{vert}{passante}};
              \draw [fill=red!50, opacity=0.5] (0, 1.5) rectangle (2, -6);
              \draw [fill=red!50, opacity=0.5] (4, 1.5) rectangle (6, -6);
              \draw [fill=vert, opacity=0.5] (2, 1.5) rectangle (4, -6);
              \draw [fill=vert, opacity=0.5] (6, 1.5) rectangle (8, -6);
            \end{scope}
          \end{pgfonlayer}
        \end{tikztimingtable}%
      }
    \end{column}
  \end{columns}
\end{frame}

\subsection{Logique asynchrone}

\begin{frame}{Logique asynchrone}
  La logique \emph{asynchrone} utilise les \emph{verrous} comme éléments de mémorisation.
  \vfill
  \begin{center}
    \begin{tikzpicture}[thick]
      \node [combinatoire] (comb_in_state) {Logique\\combinatoire};
      \node [combinatoire, right=2 of comb_in_state] (comb_state_out) {Logique\\combinatoire};
      \node [left=1 of comb_in_state, anchor=south] (entrees) {Entrées};
      \draw [-latex] (entrees.south) -- (comb.west);
      \node [right=1 of comb_state_out, anchor=south] (sorties) {Sorties};
      \draw [-latex] (comb_state_out.east) -- (sorties.south);
      \node [draw, align=center, minimum size=1.5cm] at ($(comb_in_state)!0.5!(comb_state_out)$) (etat) {Verrous};
      \draw [-latex] (comb_in_state) -- (etat);
      \draw [-latex] (etat) -- (comb_state_out);
      \draw [-latex] (etat.south) -- ++(0, -1) coordinate (down) -- (down -| entrees) -- (entrees |- comb_in_state.200) -- (comb_in_state.200);
      \draw [-latex, color=red] (comb_in_state) -- ++(0, 1.5) coordinate (ena_LR) -- node [midway, anchor=south] {$ena$} (ena_LR -| etat) -- (etat);
    \end{tikzpicture}
  \end{center}
  \vfill
  Il est \emph{très difficile} de générer correctement les signaux $ena$ :
  \begin{itemize}
  \item \emph{quand} passer de 0 à 1 ?
  \item \emph{combien de temps} rester à 1 ?
  \end{itemize}
  Nombreux \emph{paramètres} : délais de propagation, température, tension, etc...
\end{frame}

\subsection{Logique synchrone}

\begin{frame}{Logique synchrone}
  La logique \emph{synchrone} résoud ces problèmes en activant \emph{tous} les éléments de mémorisation du système \emph{simultanément}, et pour une \emph{durée très courte}.
  \vfill
  On dispose pour cela d'un \emph{signal d'horloge} :
  \begin{itemize}
  \item Fréquence fixée et stable,
  \item Rapport cyclique \prof{\SI{50}{\percent}}.
  \end{itemize}
  \vfill
  \centering
  \begin{tikztimingtable}[
    ultra thick,
    xscale=3,
    yscale=1.5,
    timing/slope=0,
    timing/rowdist=0.75cm,
    timing/coldist=2pt,
    timing/c/no arrows
    ]
    \texttt{clk} & 0.2L26{c}\\
    \extracode
    \begin{pgfonlayer}{background}
      \begin{scope}[thick,gray]
        \horlines{1}
        \vertlines[dashed] {0.2,...,13.2}
      \end{scope}
    \end{pgfonlayer}
  \end{tikztimingtable}%
\end{frame}

\begin{frame}{Bascule D}
  Le \emph{verrou} D était sensible au \emph{niveau} du signal $ena$.         \\
  La \emph{bascule} D est sensible aux \emph{fronts montants} du signal $clk$.
  \vfill
  \begin{columns}
    \begin{column}{0.3\textwidth}
      \centering
      Table de vérité                                                  \\~\\
      \renewcommand{\arraystretch}{1.25}
      \begin{tabular}{|c|c||c|c|}
        \hline
        $clk$          & $D$  & $Q$          & $\overline{Q}$          \\
        \hline
        0              & x    & \prof{$Q^-$} & \prof{$\overline{Q}^-$} \\
        \hline
        \one           & x    & \prof{$Q^-$} & \prof{$\overline{Q}^-$} \\
        \hline
        \faArrowDown   & x    & \prof{$Q^-$} & \prof{$\overline{Q}^-$} \\
        \hline
        \faArrowUp     & 0    & \prof{0}     & \prof{1}                \\
        \hline
        \faArrowUp     & \one & \prof{1}     & \prof{0}                \\
        \hline
      \end{tabular}
    \end{column}
    \begin{column}{0.7\textwidth}
      \centering
      \begin{tikzpicture}[thick]
        \node [flipflop DFF] {};
      \end{tikzpicture}                                                \\~\\
      \profetudiant{
        \begin{tikztimingtable}[
          ultra thick,
          xscale=5,
          yscale=1.5,
          timing/slope=0,
          timing/coldist=2pt,
          timing/rowdist=0.5cm,
          ]
          $clk$          & 0.2L5{C}0.2L                                  \\
          $D$            & 1L1.8H1L1.6H                                  \\
          $Q$            & 0.2U2L3.2H                                    \\
          $\overline{Q}$ & 0.2U2H3.2L                                    \\
          \extracode
          \begin{pgfonlayer}{background}
            \begin{scope}[thick,gray]
              \horlines{}
              \vertlines[dashed]{0.2,...,5.2}
            \end{scope}
          \end{pgfonlayer}
        \end{tikztimingtable}%
      }{
        \begin{tikztimingtable}[
          ultra thick,
          xscale=5,
          yscale=1.5,
          timing/slope=0,
          timing/coldist=2pt,
          timing/rowdist=0.5cm,
          ]
          $clk$          & 0.2L5{C}0.2L                                  \\
          $D$            & 1L1.8H1L1.6H                                  \\
          $Q$            & S                                    \\
          $\overline{Q}$ & S                                    \\
          \extracode
          \begin{pgfonlayer}{background}
            \begin{scope}[thick,gray]
              \horlines{}
              \vertlines[dashed]{0.2,...,5.2}
            \end{scope}
          \end{pgfonlayer}
        \end{tikztimingtable}%
      }
    \end{column}
  \end{columns}
  \vfill
  \centering
  À \emph{chaque front montant}, l'entrée $D$ est \emph{recopiée} sur la sortie $Q$.
\end{frame}

\citise{
  \begin{frame}{Architecture interne d'une bascule D}
    Une bascule D est constituée de \emph{deux verrous D} en série et d'un \emph{inverseur}.
    \vfill
    \centering
    \begin{tikzpicture}[thick]
      \node [flipflop Dlatch, scale=0.8] (amont) {};
      \node [anchor=south] at (amont.north) {amont};
      \node [flipflop Dlatch, scale=0.8, right=1 of amont] (aval) {};
      \node [anchor=south] at (aval.north) {aval};
      \node [ieeestd not port, scale=0.6, anchor=out, rotate=90] at (amont.pin 3) (not) {};
      \draw (amont.pin 6) -- (aval.pin 1);
      \node [draw, minimum width=5cm, minimum height=3.6cm] at ($(amont.pin 4)!0.5!(amont.pin 5)$) (box) {};
      \node [anchor=south west] at (aval.pin 4 -| box.east) (Qbar) {$\overline{Q}$};
      \draw [-latex] (aval.pin 4) -- (Qbar.south west) -- ++(1, 0);
      \node [anchor=south west] at (aval.pin 6 -| box.east) (Q) {$Q$};
      \draw [-latex] (aval.pin 6) -- (Q.south west) -- ++(1, 0);
      \node [anchor=south east] at (amont.pin 1 -| box.west) (D) {$D$};
      \draw [-latex] (D.south east) ++(-1, 0) -- ++(1, 0);
      \draw (D.south east) -- (amont.pin 1);
      \node [anchor=south east] at (not.in -| box.west) (clk) {$clk$};
      \draw [-latex] (clk.south east) ++(-1, 0) -- ++(1, 0);
      \draw (clk.south east) -- (not.in);
      \draw (not.in) to [short, *-] (not.in -| aval.pin 3) to (aval.pin 3);
    \end{tikzpicture}
    \vfill
    \begin{tikztimingtable}[
      ultra thick,
      xscale=14.5,
      yscale=1.5,
      timing/slope=0,
      timing/coldist=0.5pt,
      timing/rowdist=0.5cm,
      ]
      $clk$ & 0.2H2{C}0.2L\\
      $ena_{\text{\texttt{amont}}}$ & 0.25L2{C}0.15H\\
      $ena_{\text{\texttt{aval}}}$ & 0.2H2{C}0.2L\\
      \extracode
      \begin{pgfonlayer}{background}
        \begin{scope}[thick,gray]
          \horlines{}
          \vertlines[dashed]{0.2,...,5.2}
        \end{scope}
        \draw [fill=cyan, opacity=0.5] (0.25, 2) rectangle ++(0.2, -7);
        \node [color=cyan, align=center, anchor=north] at (0.2, -5.5) {enregistrement\\dans le verrou amont};
        \draw [fill=vert, opacity=0.5] (1.2, 2) rectangle ++(0.2, -7);
        \node [color=vert, align=center, anchor=north] at (1.2, -5.5) {enregistrement\\dans le verrou aval};
        \draw [fill=cyan, opacity=0.5] (2.25, 2) rectangle ++(0.2, -7);
        \node [color=cyan, align=center, anchor=north] at (2.2, -5.5) {enregistrement\\dans le verrou amont};
      \end{pgfonlayer}
    \end{tikztimingtable}%
  \end{frame}
}
  
\begin{frame}{État initial}
  Afin de partir d'un \emph{état intial connu}, on utilise un signal spécifique : \emph{reset}.\\
  Le plus souvent, ce signal est :
  \begin{itemize}
  \item asynchrone,
  \item actif à l'état bas (d'où la notation $rst\_n$).
  \end{itemize}
  Il est \emph{prioritaire} sur toutes les autres entrées de la bascule $D$.
  \vfill
  \begin{columns}
    \begin{column}{0.25\textwidth}
      \centering
      \begin{tikzpicture}[thick]
        \node [flipflop DFFrstn] {};
      \end{tikzpicture}
    \end{column}
    \begin{column}{0.74\textwidth}
      \centering
      \begin{tikztimingtable}[
        ultra thick,
        xscale=5,
        yscale=1.5,
        timing/slope=0,
        timing/coldist=2pt,
        timing/rowdist=0.5cm,
        ]
        $clk$          & 0.2L5{C}0.2L \\
        $rst\_n$          & 0.7L4.7H     \\
        $D$            & 1L1.8H1L1.6H \\
        $Q$            & 2.2L3.2H     \\
        $\overline{Q}$ & 2.2H3.2L     \\
        \extracode
        \begin{pgfonlayer}{background}
          \begin{scope}[thick,gray]
            \horlines{}
            \vertlines[dashed]{0.2,...,5.2}
          \end{scope}
        \end{pgfonlayer}
      \end{tikztimingtable}%      
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Registre}
  En accolant plusieurs bascules D, on crée un \emph{registre}.\\
  Cela permet de stocker une information de \emph{plusieurs bits}.
  \vfill
  \textbf{Exemple} : registre de 8 bits\\
  \begin{center}
    \begin{tikzpicture}[thick]
      \node [flipflop DFFrstn, ultra thick] (DFF) {};
      \draw (DFF.pin 1) ++(-1, 0) to [multiwire=8] (DFF.pin 1);
      \draw (DFF.pin 6) to [multiwire=8] ++(1, 0);
    \end{tikzpicture}
  \end{center}
\end{frame}

\subsection{Circuits synchrones classiques}

\begin{frame}{Convertisseur parallèle-série (\textit{sérialiseur})}
  \centering
  \begin{circuitikz}[thick, font=\small]
    \node [flipflop DFF, scale=0.75](DFF1) {};
    \node [mux2x1, yscale=-1, anchor=lpin 1, scale=0.75] at (DFF1.pin 6) (mux2x1_2){};
    \node [mux2x1, yscale=-1, scale=0.75, anchor=rpin 1] at (mux2x1_2 -| DFF1.pin 1) (mux2x1_1){};
    \node [flipflop DFF, anchor=pin 1, scale=0.75] at (DFF1.pin 6 -| mux2x1_2.rpin 1) (DFF2){};
    \node [mux2x1, yscale=-1, anchor=lpin 1, scale=0.75] at (DFF2.pin 6) (mux2x1_3){};
    \node [flipflop DFF, anchor=pin 1, scale=0.75] at (DFF2.pin 6 -| mux2x1_3.rpin 1) (DFF3){};
    \node [mux2x1, yscale=-1, anchor=lpin 1, scale=0.75] at (DFF3.pin 6) (mux2x1_4){};
    \node [flipflop DFF, anchor=pin 1, scale=0.75] at (DFF3.pin 6 -| mux2x1_4.rpin 1) (DFF4){};
    \foreach \i in {1,...,4}{
      \draw (mux2x1_\i.rpin 1) -- (DFF\i.pin 1);
    }
    \draw (mux2x1_1.lpin 1) to ++(0, -0.25) node [eground2, anchor=north, scale=0.75] {};
    \foreach \i/\j in {1/3,2/2,3/1,4/0}{
      \draw (mux2x1_\i.lpin 2) -- ++(0, 1) node [anchor=south] {\texttt{par(\j)}};
    }
    \node [left=0.55 of mux2x1_1.bpin 1, anchor=south east] (load) {\texttt{load}};
    \foreach \i in {1,...,4}{
      \ifthenelse{\i=4}{
        \draw (load.south east) to (mux2x1_\i.bpin 1);
      }{
        \draw (load.south east) to [short, -*] (mux2x1_\i.bpin 1) coordinate (load);
      }
    }
    \node [anchor=south west] at (DFF4.pin 6) {\texttt{ser}};
    \node [below left=1 and 0.5 of DFF1.pin 3, anchor=south east] (clk) {\texttt{clk}};
    \foreach \i in {1,...,4}{
      \ifthenelse{\i=4}{
        \draw (clk.south east) -| (DFF\i.pin 3);
      }{
        \draw (clk.south east) to [short, -*] (DFF\i.pin 3 |- clk.south east) coordinate (clk) to (DFF\i.pin 3);
      }
    }
  \end{circuitikz}
  \vfill
  \profetudiant{
    \begin{tikztimingtable}[
      ultra thick,
      xscale=5,
      yscale=1.5,
      timing/slope=0,
      timing/rowdist=0.5cm,
      timing/coldist=2pt,
      ]
      \texttt{clk}  & 0.2L12{c}0.2H      \\
      \texttt{par}  & 6.4D{\texttt{0x5}} \\
      \texttt{load} & 0.4H1L5H           \\
      \texttt{ser}  & 1.2L1H1L1H2.2L     \\
      \extracode
      \begin{pgfonlayer}{background}
        \begin{scope}[thick,gray]
          \horlines{}
          \vertlines[dashed] {0.2,...,6.2}
        \end{scope}
      \end{pgfonlayer}
    \end{tikztimingtable}%
  }{
    \begin{tikztimingtable}[
      ultra thick,
      xscale=5,
      yscale=1.5,
      timing/slope=0,
      timing/rowdist=0.5cm,
      timing/coldist=2pt,
      ]
      \texttt{clk}  & 0.2L12{c}0.2H      \\
      \texttt{par}  & 6.4D{\texttt{0x5}} \\
      \texttt{load} & 0.4H1L5H           \\
      \texttt{ser}  & S     \\
      \extracode
      \begin{pgfonlayer}{background}
        \begin{scope}[thick,gray]
          \horlines{}
          \vertlines[dashed] {0.2,...,6.2}
        \end{scope}
      \end{pgfonlayer}
    \end{tikztimingtable}%
  }
\end{frame}

\begin{frame}{Convertisseur série-parallèle (\textit{désérialiseur})}
  \centering
  \begin{circuitikz}[thick, font=\small]
    \node [flipflop DFF, scale=0.75](DFF1) {};
    \node [flipflop DFF, scale=0.75, right=0.5 of DFF1.pin 6, anchor=pin 1] (DFF2) {};
    \node [flipflop DFF, scale=0.75, right=0.5 of DFF2.pin 6, anchor=pin 1] (DFF3) {};
    \node [flipflop DFF, scale=0.75, right=0.5 of DFF3.pin 6, anchor=pin 1] (DFF4) {};

    \draw (DFF1.pin 1) -- ++(-1, 0) node [anchor=south east] {\texttt{ser}};
    \foreach \i/\j in {1/2,2/3,3/4}{
      \draw (DFF\i.pin 6) -- (DFF\j.pin 1);
    }
    \foreach \i/\j in {1/3,2/2,3/1,4/0}{
      \ifthenelse{\i=4}{
        \draw (DFF\i.pin 6) -- ++(0.25, 0) -- ++(0, -3) node [anchor=north] {\texttt{par(\j)}};
      }{
        \draw (DFF\i.pin 6) ++(0.25, 0) to [short, *-] ++(0, -3) node [anchor=north] {\texttt{par(\j)}};
      }
    }
    \node [below left=1 and 0.5 of DFF1.pin 3, anchor=south east] (clk) {\texttt{clk}};
    \foreach \i in {1,...,4}{
      \ifthenelse{\i=4}{
        \draw (clk.south east) -| (DFF\i.pin 3);
      }{
        \draw (clk.south east) to [short, -*] (DFF\i.pin 3 |- clk.south east) coordinate (clk) to (DFF\i.pin 3);
      }
    }
  \end{circuitikz}
  \vfill
  \profetudiant{
    \begin{tikztimingtable}[
      ultra thick,
      xscale=5,
      yscale=1.5,
      timing/slope=0,
      timing/rowdist=0.5cm,
      timing/coldist=2pt,
      ]
      \texttt{clk}  & 0.2L8{c}0.2H      \\
      \texttt{ser}  & 0.2L1H1L1H1L0.2L     \\
      \texttt{par}  & 0.2D{}1D{0x8}1D{0x4}1D{0xA}1D{0x5}0.2D{} \\
      \extracode
      \begin{pgfonlayer}{background}
        \begin{scope}[thick,gray]
          \horlines{}
          \vertlines[dashed] {0.2,...,4.2}
        \end{scope}
      \end{pgfonlayer}
    \end{tikztimingtable}%
  }{
    \begin{tikztimingtable}[
      ultra thick,
      xscale=5,
      yscale=1.5,
      timing/slope=0,
      timing/rowdist=0.5cm,
      timing/coldist=2pt,
      ]
      \texttt{clk}  & 0.2L8{c}0.2H      \\
      \texttt{ser}  & 0.2L1H1L1H1L0.2L     \\
      \texttt{par}  & 0.2D{}1D{}1D{}1D{}1D{}0.2D{} \\
      \extracode
      \begin{pgfonlayer}{background}
        \begin{scope}[thick,gray]
          \horlines{}
          \vertlines[dashed] {0.2,...,4.2}
        \end{scope}
      \end{pgfonlayer}
    \end{tikztimingtable}%
  }
\end{frame}

\begin{frame}{Compteur synchrone de 0 à 3}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \centering
      Table de vérité                              \\~\\
      \renewcommand{\arraystretch}{1.25}
      \begin{tabular}{|c|c||c|c|}
        \hline
        $C_1^-$ & $C_0^-$ & $C_1$    & $C_0$       \\
        \hline
        0       & 0       & \prof{0} & \prof{1}    \\
        \hline
        0       & \one    & \prof{1} & \prof{0}    \\
        \hline
        \one    & 0       & \prof{1} & \prof{1}    \\
        \hline
        \one    & \one    & \prof{0} & \prof{0}    \\
        \hline
      \end{tabular}                                \\
    \end{column}
    \begin{column}{0.5\textwidth}
      \centering
      $C_0 = \prof{\overline{C_0^-}}$              \\
      \vspace{1cm}
      $C_1 = \prof{C_0^- \cdot \overline{C_1^-} + \overline{C_0^-} \cdot C_1^-$}
    \end{column}
  \end{columns}
  \vfill
  \centering
  \profetudiant{
    \begin{tikztimingtable}[
      ultra thick,
      xscale=5,
      yscale=1.5,
      timing/slope=0,
      timing/rowdist=0.5cm,
      timing/coldist=2pt,
      ]
      $clk$    & 0.2L14{c}0.2H                     \\
      $rst\_n$ & 0.5L6.9H                          \\
      $C$      & 1.2D{0}D{1}D{2}D{3}D{0}D{1}D{2}0.2D{} \\
      $C_1$    & 2.2L2H2L1H0.2H                      \\
      $C_0$    & 1.2L1H1L1H1L1H1L0.2H                \\
      \extracode
      \begin{pgfonlayer}{background}
        \begin{scope}[thick,gray]
          \horlines{}
          \vertlines[dashed] {0.2,...,7.2}
        \end{scope}
      \end{pgfonlayer}
    \end{tikztimingtable}%
  }{%
    \begin{tikztimingtable}[
      ultra thick,
      xscale=5,
      yscale=1.5,
      timing/slope=0,
      timing/rowdist=0.5cm,
      timing/coldist=2pt,
      ]
      $clk$    & 0.2L14{c}0.2H                     \\
      $rst\_n$ & 0.5L6.9H                          \\
      $C$      &                                   \\
      $C_1$    &                                   \\
      $C_0$    &                                   \\
      \extracode
      \begin{pgfonlayer}{background}
        \begin{scope}[thick,gray]
          \horlines{}
          \vertlines[dashed] {0.2,...,7.2}
        \end{scope}
      \end{pgfonlayer}
    \end{tikztimingtable}%
  }
\end{frame}

\begin{frame}{Compteur synchrone de 0 à 3}
  \centering
  \begin{tikzpicture}[thick]
    \node [flipflop DFFrstn] (C0) {$C_0$};
    \node [flipflop DFFrstn, below=0.5 of C0] (C1) {$C_1$};
    \profetudiant{
      \draw (C0.pin 6) to [short, *-] ++(1.5, 0) node [anchor=south west] {$C_0$};
      \draw (C1.pin 6) to ++ (0.5, 0) to [short, *-] ++(1, 0) node [anchor=south west] {$C_1$};
    }{
      \draw (C0.pin 6) to ++(1.5, 0) node [anchor=south west] {$C_0$};
      \draw (C1.pin 6) to ++(1.5, 0) node [anchor=south west] {$C_1$};
    }
    \draw (C0.pin 3) -- (C1.pin 3) to [short, *-] ++(0, 0) to ++(-3, 0) node [anchor=south east] (clk) {$clk$};
    
    \draw (C0.down) -| (C0.down -| C0.pin 4) |- (C1.down) to [short, *-] ++(0, 0) coordinate (tmp) to (tmp -| clk.south east) node [anchor=south east] {$rst\_n$};
    \profetudiant{
      \node [ieeestd or port, anchor=out, scale=0.75] at (C1.pin 1) (OR) {};
      \node [ieeestd and port, scale=0.75, above left=0 and 0 of OR.in 1] (ANDh) {};
      \draw (ANDh.out) -- (OR.in 1);
      \node [ieeestd and port, scale=0.75, below left=0 and 0 of OR.in 2] (ANDl) {};
      \draw (ANDl.out) -- (OR.in 2);
      
      \coordinate [above left=0.25 and 3.5 of C0.north west] (C0sig) {};
      \coordinate [above left=0.25 and 0.25 of C0sig] (notC0sig) {};
      \coordinate [above left=0.25 and 0.25 of notC0sig] (C1sig) {};
      \coordinate [above left=0.25 and 0.25 of C1sig] (notC1sig) {};
      
      \draw (C0.pin 6) |- (C0sig) |- (ANDh.in 1);
      \draw (C0.pin 4) to ++(0.25, 0) |- (notC0sig) |- (ANDl.in 1);
      \draw (C1.pin 6) to ++(0.5, 0) |- (C1sig) |- (ANDl.in 2);
      \draw (C1.pin 4) to ++(0.75, 0) |- (notC1sig) |- (ANDh.in 2);
      
      \draw (notC0sig) to ++ (1, 0) coordinate (tmp) to [short, *-] (tmp |- C0.pin 1) to (C0.pin 1);
    }{
      \node [inner sep=0, above=1 of C0.north] {};
      \draw (C1.pin 4) to ++(0.25, 0);
      \draw (C1.pin 1) to ++(-0.25, 0);
    }
  \end{tikzpicture}
\end{frame}

\begin{limite}
  Les circuits séquentiels deviennent rapidement\\difficiles à décrire avec des logigrammes.
\end{limite}