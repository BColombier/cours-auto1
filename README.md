# Versions

Trois versions du diapo :

  - `diapo` : celui à projeter, à compléter
  - `etudiant` : livret distribué, à compléter
  - `prof` : référence, avec tout le contenu

# Compilation

```
latexmk -pvc -shell-escape diapo.tex
latexmk -pvc -shell-escape etudiant.tex
latexmk -pvc -shell-escape prof.tex
```
